﻿ @WES_FTR_Page_CreateForecasts  @WES_FTR_USTEST
Feature: WES_Page_CreateForecasts 
	In order to check create forecast page
	I visit and check details

@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check create forecasts page access for logged in user 
  Given I login to the WES portal
   When I redirect to the create forecasts page 
   Then <PageTitle> should be dispayed on page

	Examples: 
|   PageTitle   |
|CREATE FORECAST|

@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check result with blank ISO, blank Utility, blank Congestion Zone, default Start Date, default End Date 
  Given I login to the WES portal
   When I redirect to the create forecasts page 
    And click on the Create Forecast search button
   Then The <ErrorMessage> should be dispayed on the Create Forecast page

	Examples: 
|ErrorMessage|
| Supply ISO.|

	@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check error messages with valid ISO, blank Congestion Zone, default Start Date, default End Date 
  Given I login to the WES portal 
   When I redirect to the create forecasts page  
    And Select <SelectIOS> ISO from dropdown 
    And click on the Create Forecast search button 
   Then The <ErrorMessage> should be dispayed on the Create Forecast page 

	Examples: 
|SelectIOS|   ErrorMessage  |
|  ERCOT  |No Results Found.|


	@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check result with valid ISO, blank Congestion Zone, default Start Date, default End Date 
  Given I login to the WES portal 
   When I redirect to the create forecasts page  
    And Select <SelectIOS> ISO from dropdown 
    And click on the Create Forecast search button 
   Then ResultList should be dispayed on the Create Forecast page 

	Examples: 
|SelectIOS|
|   MISO  |
|  PJMISO |

	@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check result with valid ISO, valid Utility, blank Congestion Zone, default Start Date, default End Date 
  Given I login to the WES portal 
   When I redirect to the create forecasts page  
    And Select <SelectIOS> ISO from dropdown 
    And Select <SelectUtility> Utility from dropdown
    And click on the Create Forecast search button 
   Then ResultList should be dispayed on the Create Forecast page 
	
	Examples: 
|SelectIOS|   SelectUtility  |
|   MISO  |      AMEREN      |
|  PJMISO |ALLEGHENY POWER PA|


	@WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check result with valid ISO, valid Utility, valid Congestion Zone, default Start Date, default End Date 
  Given I login to the WES portal 
   When I redirect to the create forecasts page  
    And Select <SelectIOS> ISO from dropdown 
    And Select <SelectUtility> Utility from dropdown
    And Select <SelectCongestionZone> Congestion Zone from dropdown
    And click on the Create Forecast search button 
   Then ResultList should be dispayed on the Create Forecast page 
	
	Examples: 
|SelectIOS|   SelectUtility  |SelectCongestionZone|
|   MISO  |      AMEREN      |      ILLINOIS      |
|  PJMISO |ALLEGHENY POWER PA|         APS        |
	 

	 @WES_SNR_CreateForecast @WES_SNR_USTEST
Scenario Outline: Check result with valid ISO, valid Utility, valid Congestion Zone, Start Date greater than End Date, default End Date 
  Given I login to the WES portal 
   When I redirect to the create forecasts page  
    And Select <SelectIOS> ISO from dropdown 
    And Select <SelectUtility> Utility from dropdown
    And Select <SelectCongestionZone> Congestion Zone from dropdown
    And Enter Start Date greater than End Date
    And click on the Create Forecast search button 
   Then The <ErrorMessage> should be dispayed on the Create Forecast page 
	
	Examples: 
|SelectIOS|SelectUtility|SelectCongestionZone|                  ErrorMessage                 |
|   MISO  |    AMEREN   |      ILLINOIS      |Supply End Date Equal or Later tham Start Date.|
	 