// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace WES_TEST_Automation.Features.PageAccess
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("WES_Page_CreateForecasts", new string[] {
            "WES_FTR_Page_CreateForecasts",
            "WES_FTR_USTEST"}, Description="\tIn order to check create forecast page\r\n\tI visit and check details", SourceFile="Features\\PageAccess\\WES_Page_CreateForecasts.feature", SourceLine=1)]
    public partial class WES_Page_CreateForecastsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "WES_FTR_Page_CreateForecasts",
                "WES_FTR_USTEST"};
        
#line 1 "WES_Page_CreateForecasts.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-NZ"), "WES_Page_CreateForecasts", "\tIn order to check create forecast page\r\n\tI visit and check details", ProgrammingLanguage.CSharp, new string[] {
                        "WES_FTR_Page_CreateForecasts",
                        "WES_FTR_USTEST"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void CheckCreateForecastsPageAccessForLoggedInUser(string pageTitle, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check create forecasts page access for logged in user", null, @__tags);
#line 7
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 8
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 9
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 10
   testRunner.Then(string.Format("{0} should be dispayed on page", pageTitle), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check create forecasts page access for logged in user, CREATE FORECAST", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=13)]
        public virtual void CheckCreateForecastsPageAccessForLoggedInUser_CREATEFORECAST()
        {
#line 7
this.CheckCreateForecastsPageAccessForLoggedInUser("CREATE FORECAST", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckResultWithBlankISOBlankUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate(string errorMessage, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check result with blank ISO, blank Utility, blank Congestion Zone, default Start " +
                    "Date, default End Date", null, @__tags);
#line 17
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 18
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 19
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 20
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 21
   testRunner.Then(string.Format("The {0} should be dispayed on the Create Forecast page", errorMessage), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with blank ISO, blank Utility, blank Congestion Zone, default Start " +
            "Date, default End Date, Supply ISO.", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=24)]
        public virtual void CheckResultWithBlankISOBlankUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate_SupplyISO_()
        {
#line 17
this.CheckResultWithBlankISOBlankUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate("Supply ISO.", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckErrorMessagesWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate(string selectIOS, string errorMessage, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check error messages with valid ISO, blank Congestion Zone, default Start Date, d" +
                    "efault End Date", null, @__tags);
#line 28
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 29
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 30
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 31
    testRunner.And(string.Format("Select {0} ISO from dropdown", selectIOS), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 32
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 33
   testRunner.Then(string.Format("The {0} should be dispayed on the Create Forecast page", errorMessage), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check error messages with valid ISO, blank Congestion Zone, default Start Date, d" +
            "efault End Date, ERCOT", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=36)]
        public virtual void CheckErrorMessagesWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate_ERCOT()
        {
#line 28
this.CheckErrorMessagesWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate("ERCOT", "No Results Found.", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckResultWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate(string selectIOS, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check result with valid ISO, blank Congestion Zone, default Start Date, default E" +
                    "nd Date", null, @__tags);
#line 41
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 42
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 43
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 44
    testRunner.And(string.Format("Select {0} ISO from dropdown", selectIOS), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 45
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 46
   testRunner.Then("ResultList should be dispayed on the Create Forecast page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, blank Congestion Zone, default Start Date, default E" +
            "nd Date, MISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=49)]
        public virtual void CheckResultWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate_MISO()
        {
#line 41
this.CheckResultWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate("MISO", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, blank Congestion Zone, default Start Date, default E" +
            "nd Date, PJMISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=49)]
        public virtual void CheckResultWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate_PJMISO()
        {
#line 41
this.CheckResultWithValidISOBlankCongestionZoneDefaultStartDateDefaultEndDate("PJMISO", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckResultWithValidISOValidUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate(string selectIOS, string selectUtility, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check result with valid ISO, valid Utility, blank Congestion Zone, default Start " +
                    "Date, default End Date", null, @__tags);
#line 54
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 55
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 56
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 57
    testRunner.And(string.Format("Select {0} ISO from dropdown", selectIOS), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 58
    testRunner.And(string.Format("Select {0} Utility from dropdown", selectUtility), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 59
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 60
   testRunner.Then("ResultList should be dispayed on the Create Forecast page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, valid Utility, blank Congestion Zone, default Start " +
            "Date, default End Date, MISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=63)]
        public virtual void CheckResultWithValidISOValidUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate_MISO()
        {
#line 54
this.CheckResultWithValidISOValidUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate("MISO", "AMEREN", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, valid Utility, blank Congestion Zone, default Start " +
            "Date, default End Date, PJMISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=63)]
        public virtual void CheckResultWithValidISOValidUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate_PJMISO()
        {
#line 54
this.CheckResultWithValidISOValidUtilityBlankCongestionZoneDefaultStartDateDefaultEndDate("PJMISO", "ALLEGHENY POWER PA", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckResultWithValidISOValidUtilityValidCongestionZoneDefaultStartDateDefaultEndDate(string selectIOS, string selectUtility, string selectCongestionZone, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check result with valid ISO, valid Utility, valid Congestion Zone, default Start " +
                    "Date, default End Date", null, @__tags);
#line 69
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 70
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 71
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 72
    testRunner.And(string.Format("Select {0} ISO from dropdown", selectIOS), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 73
    testRunner.And(string.Format("Select {0} Utility from dropdown", selectUtility), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 74
    testRunner.And(string.Format("Select {0} Congestion Zone from dropdown", selectCongestionZone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 75
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 76
   testRunner.Then("ResultList should be dispayed on the Create Forecast page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, valid Utility, valid Congestion Zone, default Start " +
            "Date, default End Date, MISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=79)]
        public virtual void CheckResultWithValidISOValidUtilityValidCongestionZoneDefaultStartDateDefaultEndDate_MISO()
        {
#line 69
this.CheckResultWithValidISOValidUtilityValidCongestionZoneDefaultStartDateDefaultEndDate("MISO", "AMEREN", "ILLINOIS", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, valid Utility, valid Congestion Zone, default Start " +
            "Date, default End Date, PJMISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=79)]
        public virtual void CheckResultWithValidISOValidUtilityValidCongestionZoneDefaultStartDateDefaultEndDate_PJMISO()
        {
#line 69
this.CheckResultWithValidISOValidUtilityValidCongestionZoneDefaultStartDateDefaultEndDate("PJMISO", "ALLEGHENY POWER PA", "APS", ((string[])(null)));
#line hidden
        }
        
        public virtual void CheckResultWithValidISOValidUtilityValidCongestionZoneStartDateGreaterThanEndDateDefaultEndDate(string selectIOS, string selectUtility, string selectCongestionZone, string errorMessage, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_CreateForecast",
                    "WES_SNR_USTEST"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Check result with valid ISO, valid Utility, valid Congestion Zone, Start Date gre" +
                    "ater than End Date, default End Date", null, @__tags);
#line 85
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 86
  testRunner.Given("I login to the WES portal", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 87
   testRunner.When("I redirect to the create forecasts page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 88
    testRunner.And(string.Format("Select {0} ISO from dropdown", selectIOS), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 89
    testRunner.And(string.Format("Select {0} Utility from dropdown", selectUtility), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 90
    testRunner.And(string.Format("Select {0} Congestion Zone from dropdown", selectCongestionZone), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 91
    testRunner.And("Enter Start Date greater than End Date", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 92
    testRunner.And("click on the Create Forecast search button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line hidden
#line 93
   testRunner.Then(string.Format("The {0} should be dispayed on the Create Forecast page", errorMessage), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Check result with valid ISO, valid Utility, valid Congestion Zone, Start Date gre" +
            "ater than End Date, default End Date, MISO", new string[] {
                "WES_SNR_CreateForecast",
                "WES_SNR_USTEST"}, SourceLine=96)]
        public virtual void CheckResultWithValidISOValidUtilityValidCongestionZoneStartDateGreaterThanEndDateDefaultEndDate_MISO()
        {
#line 85
this.CheckResultWithValidISOValidUtilityValidCongestionZoneStartDateGreaterThanEndDateDefaultEndDate("MISO", "AMEREN", "ILLINOIS", "Supply End Date Equal or Later tham Start Date.", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
