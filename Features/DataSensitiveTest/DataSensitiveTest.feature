﻿@WES_FTR_DataSensitiveTest  @WES_FTR_USTEST
Feature: DataSensitiveTest
	In order to check updated data 
	I want to be check database

@WES_SNR_DataSensitiveTest @WES_SNR_USTEST
Scenario Outline: Initial data sensitive test
  Given Connect to the datatbase ORATESTDB
    And Execute the Sql query <QueryString> and <TestDate> to get the result set from <DataType>
   When Compare I compare the result set
   Then the result should be displayed

	Examples: 
|                                                                                                                                                                                                                                                                            QueryString                                                                                                                                                                                                                                                                           | TestDate |DataType|
|                                                                                          SELECT SS_ACCOUNT.SS_ACCT_ID,SS_ACCOUNT.OPP_START_DATE, NONIDRKH.REC_ID, NONIDRKH.SVC_END_DATE FROM SS_ACCOUNT INNER JOIN NONIDRKH ON (NONIDRKH.SS_ACCT_ID = SS_ACCOUNT.SS_ACCT_ID)  WHERE  SS_ACCOUNT.OPP_END_DATE IS NULL AND  SS_ACCOUNT.OPP_START_DATE <  <TestStartDate> AND  NONIDRKH.SVC_END_DATE < <TestEndDate> AND  ROWNUM <= 100 order by NONIDRKH.SVC_END_DATE DESC                                                                                         |2017-12-01|NONIDRKH|
|SELECT SS_ACCOUNT.SS_ACCT_ID, IDRKH.REC_ID, IDRKH.IDR_DATE FROM SS_ACCOUNT LEFT JOIN IDRKH ON (IDRKH.SS_ACCT_ID = SS_ACCOUNT.SS_ACCT_ID) WHERE SS_ACCOUNT.OPP_END_DATE IS NULL AND SS_ACCOUNT.OPP_START_DATE < <TestStartDate> AND IDRKH.IDR_DATE < <TestEndDate> AND ROWNUM <= 100 AND IDRKH.SS_ACCT_ID NOT IN(SELECT SS_ACCOUNT.SS_ACCT_ID FROM SS_ACCOUNT INNER JOIN NONIDRKH ON (NONIDRKH.SS_ACCT_ID = SS_ACCOUNT.SS_ACCT_ID) WHERE SS_ACCOUNT.OPP_END_DATE IS NULL AND NONIDRKH.SVC_END_DATE < <TestEndDate> AND ROWNUM <= 100)  order by IDRKH.IDR_DATE DESC|2017-12-01|  IDRKH |
 