﻿﻿@WES_FTR_ChangePassword @WES_FTR_USTEST
Feature: WES_FTR_ChangePassword
	For the security reason 
	I want to be change my password
// LATEST UPDATES
@WES_SNR_ChangePassword @WES_SNR_USTEST
Scenario Outline: Change password with Invalid old password, valid new password and valid confirm password
  Given I enter <OldPassword>
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then The <Message> should be display on the page

	Examples: 
|  OldPassword | NewPassword|ConfirmPassword|              Message              |
|Norman--199011|Norman--1990|  Norman--1990 |Please enter valid current password|

	@WES_SNR_ChangePassword @WES_SNR_USTEST
    Scenario Outline: Change password with valid old password, Invalid new password and valid confirm password
  Given I have enter valid old password
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then The <Message> should be display on the page

	Examples: 
|NewPassword|ConfirmPassword|                               Message                              |
| 123456789 |  Norman--1990 |Your new password and confirmed password do not match. Please retry.|


	@WES_SNR_ChangePassword @WES_SNR_USTEST
    Scenario Outline: Change password with valid old password, valid new password and Invalid confirm password
  Given I have enter valid old password
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then The <Message> should be display on the page

	Examples: 
| NewPassword|ConfirmPassword|                               Message                              |
|Norman--1990|   123456789   |Your new password and confirmed password do not match. Please retry.|



	@WES_SNR_ChangePassword @WES_SNR_USTEST
    Scenario Outline: Change password with valid old password, Invalid new password Pattern and valid confirm password
  Given I have enter valid old password
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then The <Message> should be display on the page

	Examples: 
|  NewPassword |ConfirmPassword|                    Message                   |
| NormanNorman |  NormanNorman |Password does not meet complexity requirements|
#| '------NNNNNN| '------NNNNNN |Password does not meet complexity requirements|
#| '------MMMMMM| '------MMMMMM |Password does not meet complexity requirements|
#|'------MMMMMM-| '------MMMMMM-|Password does not meet complexity requirements|
#| '000000------| '000000------ |Password does not meet complexity requirements|
#|'000000------@| '000000------@|Password does not meet complexity requirements|
#| NNNNNNNNNNNN |  NNNNNNNNNNNN |Password does not meet complexity requirements|
#| NNNNNNNNNNnn |  NNNNNNNNNNnn |Password does not meet complexity requirements|
	


	@WES_SNR_ChangePassword @WES_SNR_USTEST
    Scenario Outline: Change password with valid old password, valid new password recently used and valid confirm password
  Given I have enter valid old password
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then The <Message> should be display on the page

	Examples: 
|  NewPassword  |ConfirmPassword|                              Message                              |
|tCognition@1113|tCognition@1113|Recent passwords cannot be used. Please enter a different password.|

	@WES_SNR_ChangePassword @WES_SNR_USTEST
    Scenario Outline: Change password with valid old password, valid new password and valid confirm password
  Given I have enter valid old password
    And I entered the <NewPassword> field
    And I entered the <ConfirmPassword> field to confirm
   When I press change password button
   Then Success <Message> should be display on the page

	Examples: 
| NewPassword     | ConfirmPassword | Message                       |
| tCognition@1217 | tCognition@1217 | Password changed successfully |
#| tCognition@1010 | tCognition@1010 | Password changed successfully |
#| '------NNNNNNn  | '------NNNNNNn  | Password changed successfully |
#| '------MMMMMM0  | '------MMMMMM0  | Password changed successfully |
#| NNNNNNNNNNn0N   | NNNNNNNNNNn0N   | Password changed successfully |
 