﻿@WES_FTR_NavigateToMenu  @WES_FTR_USTEST @WES_FTR_JAPANTEST
Feature: WES_FTR_NavigateToMenu
	In order to access pages
	I navigate to several menus

@WES_SNR_NavigateToMenu  @WES_SNR_USTEST @WES_SNR_NavigateToMenu_CreateForecast
Scenario Outline: Navigate to the WES portal menus
	Given I hover to <MenuItem>
	When I click on the dropdown <MenuOption>
	Then <PageTitle> should be dispayed on page

	Examples: 
	| MenuItem  | MenuOption      | PageTitle       |
	| FORECASTS | Create Forecast | CREATE FORECAST |
	 
 
 @WES_SNR_NavigateToMenu  @WES_SNR_JAPANTEST @WES_SNR_NavigateToMenu_CreateForecast
Scenario Outline: Navigate to the Japan WES portal menus
	Given I hover to <MenuItem>
	When I click on the dropdown <MenuOption>
	Then <PageTitle> should be dispayed on page

	Examples: 
	| MenuItem | MenuOption      | PageTitle       |
	| FORECAST | Create Forecast | CREATE FORECAST |