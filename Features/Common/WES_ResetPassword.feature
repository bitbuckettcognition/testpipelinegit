﻿@WES_FTR_ResetPassword  @WES_FTR_USTEST
Feature: WES_FTR_ResetPassword
	Im forgot my password 
	I want to reset my password

	@WES_SNR_ResetPassword @WES_SNR_USTEST
Scenario Outline: Reset password with blank username  
	Given I redirect to the WES portal
	And enter an <Username> to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box

	Examples: 
	| Username | PopupMessage                             |
	|          | Please enter your User ID and try again. |



	@WES_SNR_ResetPassword @WES_SNR_USTEST
Scenario Outline: Reset password with Invalid username
	Given I redirect to the WES portal
	And enter an <Username> to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box

	Examples: 
	| Username | PopupMessage                                                                                |
	| XYZ      | An email will be sent to your email address for XYZ. It will contain a password reset link. |



	@WES_SNR_ResetPassword @WES_SNR_USTEST
Scenario Outline: Reset password with valid username
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box

	Examples: 
	| PopupMessage                                                                                            |
	| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. |



@WES_SNR_ResetPassword @WES_SNR_USTEST
Scenario Outline: Reset password with Invalid new password and Invalid confirm password
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box
	Then I open an email and click on the reset password link
	When I entered the <NewPassword> field on page
	And I entered the <ConfirmPassword> field to confirm password
	When I press reset password button
	Then The <Message> should be display

	Examples: 
	| PopupMessage                                                                                            | NewPassword | ConfirmPassword | Message                                                              |
	| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | 1234567890  | 123456789       | Your new password and confirmed password do not match. Please retry. |

	@WES_SNR_ResetPassword @WES_SNR_USTEST
    Scenario Outline: Reset password with valid new password and Invalid confirm password
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box
	Then I open an email and click on the reset password link
	When I entered the <NewPassword> field on page
	And I entered the <ConfirmPassword> field to confirm password
	When I press reset password button
	Then The <Message> should be display

	Examples: 
	| PopupMessage                                                                                            | NewPassword   | ConfirmPassword | Message                                                              |
	| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | nEtwork$2223! | 123456789       | Your new password and confirmed password do not match. Please retry. |

	@WES_SNR_ResetPassword @WES_SNR_USTEST
    Scenario Outline: Reset password with Invalid new password pattern and valid confirm password
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box
	Then I open an email and click on the reset password link
	When I entered the <NewPassword> field on page
	And I entered the <ConfirmPassword> field to confirm password
	When I press reset password button
	Then The <Message> should be display

	Examples: 
	| PopupMessage                                                                                            | NewPassword    | ConfirmPassword | Message                                        |
	| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | NormanNorman   | NormanNorman    | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '------NNNNNN  | '------NNNNNN   | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '------MMMMMM  | '------MMMMMM   | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '------MMMMMM- | '------MMMMMM-  | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '000000------  | '000000------   | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '000000------@ | '000000------@  | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | NNNNNNNNNNNN   | NNNNNNNNNNNN    | Password does not meet complexity requirements |
	#| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | NNNNNNNNNNnn   | NNNNNNNNNNnn    | Password does not meet complexity requirements |
	

	@WES_SNR_ResetPassword @WES_SNR_USTEST
    Scenario Outline: Reset password with valid new password recently used and valid confirm password
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box
	Then I open an email and click on the reset password link
	When I entered the <NewPassword> field on page
	And I entered the <ConfirmPassword> field to confirm password
	When I press reset password button
	Then The <Message> should be display

	Examples: 
	| PopupMessage                                                                                            | NewPassword      | ConfirmPassword  | Message                                                             |
	| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | tCognition@12345 | tCognition@12345 | Recent passwords cannot be used. Please enter a different password. |

	@WES_SNR_ResetPassword @WES_SNR_USTEST
    Scenario Outline: Reset password with valid new password and valid confirm password
	Given I redirect to the WES portal
	And enter a valid username to get reset password link
	And I click on the forgot my password link  
	Then The <PopupMessage> should be display on alert box
	Then I open an email and click on the reset password link
	When I entered the <NewPassword> field on page
	And I entered the <ConfirmPassword> field to confirm password
	When I press reset password button
	Then Success label <Message> should be display

	Examples: 
    | PopupMessage                                                                                            | NewPassword    | ConfirmPassword | Message                       |
    | An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | tCognition@1116   | tCognition@1116    | Password changed successfully |
    #| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | Norman19901990 | Norman19901990  | Password changed successfully |
    #| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '------NNNNNNn | '------NNNNNNn  | Password changed successfully |
    #| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | '------MMMMMM0 | '------MMMMMM0  | Password changed successfully |
    #| An email will be sent to your email address for TCOGNITION_TEST. It will contain a password reset link. | NNNNNNNNNNn0   | NNNNNNNNNNn0    | Password changed successfully |
	

