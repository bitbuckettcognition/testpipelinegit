﻿@WES_FTR_PortalLogin  @WES_FTR_USTEST 
Feature: WES_FTR_PortalLogin
	In order to start the automation
	I need to login to WES portal  

    @WES_SNR_PortalLogin  @WES_SNR_USTEST @WES_SNR_PortalLogin_SuccessfullLoginWithValidCredentials 
    Scenario Outline: Successfully login to WES portal with valid username and password
	Given I redirect to the WES portal
	And Enter the valid credentials
	When I clicked on the login button
	Then Profile Name should be dispayed on the home page
	 


	@WES_SNR_PortalLogin  @WES_SNR_USTEST @WES_SNR_PortalLogin_WithBlankUsernamePassword
    Scenario Outline: Login to WES portal with blank username and password
	Given I redirect to the WES portal
	And I login to the WES with <Username> and <Password> on login page
	When I clicked on the login button
	Then The <ErrorMessage> should be dispayed on the login page

	Examples: 
	| Username | Password | ErrorMessage        |
	|          |          | Fields are required |
	 

	@WES_SNR_PortalLogin @WES_SNR_USTEST @WES_SNR_PortalLogin_WithValidUsernameAndInvalidPassword
    Scenario Outline: Login to WES portal with valid username and invalid password
	Given I redirect to the WES portal
	And I login to the WES portal with valid username and invalid <Password> on login page
	When I clicked on the login button
	Then The <ErrorMessage> should be dispayed on the login page

	Examples: 
	| Password        | ErrorMessage                       |
	| NNNNNNNNNNn0N11 | Invalid User ID or Password syntax |
	 

	@WES_SNR_PortalLogin @WES_SNR_USTEST @WES_SNR_PortalLogin_WithInvalidUsernameAndValidPassword
    Scenario Outline: Login to WES portal with invalid username and valid password
	Given I redirect to the WES portal
	And I login to the WES portal with Invalid <Username> and valid password on login page
	When I clicked on the login button
	Then The <ErrorMessage> should be dispayed on the login page

	Examples: 
	| Username | ErrorMessage                       |
	| XYZ      | Invalid User ID or Password syntax |

	 