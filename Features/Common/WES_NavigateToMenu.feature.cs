// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:3.1.0.0
//      SpecFlow Generator Version:3.1.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace WES_TEST_Automation.Features.Common
{
    using TechTalk.SpecFlow;
    using System;
    using System.Linq;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "3.1.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [TechTalk.SpecRun.FeatureAttribute("WES_FTR_NavigateToMenu", new string[] {
            "WES_FTR_NavigateToMenu",
            "WES_FTR_USTEST",
            "WES_FTR_JAPANTEST"}, Description="\tIn order to access pages\r\n\tI navigate to several menus", SourceFile="Features\\Common\\WES_NavigateToMenu.feature", SourceLine=1)]
    public partial class WES_FTR_NavigateToMenuFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
        private string[] _featureTags = new string[] {
                "WES_FTR_NavigateToMenu",
                "WES_FTR_USTEST",
                "WES_FTR_JAPANTEST"};
        
#line 1 "WES_NavigateToMenu.feature"
#line hidden
        
        [TechTalk.SpecRun.FeatureInitialize()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-NZ"), "WES_FTR_NavigateToMenu", "\tIn order to access pages\r\n\tI navigate to several menus", ProgrammingLanguage.CSharp, new string[] {
                        "WES_FTR_NavigateToMenu",
                        "WES_FTR_USTEST",
                        "WES_FTR_JAPANTEST"});
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [TechTalk.SpecRun.FeatureCleanup()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        [TechTalk.SpecRun.ScenarioCleanup()]
        public virtual void TestTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void NavigateToTheWESPortalMenus(string menuItem, string menuOption, string pageTitle, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_NavigateToMenu",
                    "WES_SNR_USTEST",
                    "WES_SNR_NavigateToMenu_CreateForecast"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Navigate to the WES portal menus", null, @__tags);
#line 7
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 8
 testRunner.Given(string.Format("I hover to {0}", menuItem), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 9
 testRunner.When(string.Format("I click on the dropdown {0}", menuOption), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 10
 testRunner.Then(string.Format("{0} should be dispayed on page", pageTitle), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Navigate to the WES portal menus, FORECASTS", new string[] {
                "WES_SNR_NavigateToMenu",
                "WES_SNR_USTEST",
                "WES_SNR_NavigateToMenu_CreateForecast"}, SourceLine=13)]
        public virtual void NavigateToTheWESPortalMenus_FORECASTS()
        {
#line 7
this.NavigateToTheWESPortalMenus("FORECASTS", "Create Forecast", "CREATE FORECAST", ((string[])(null)));
#line hidden
        }
        
        public virtual void NavigateToTheJapanWESPortalMenus(string menuItem, string menuOption, string pageTitle, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "WES_SNR_NavigateToMenu",
                    "WES_SNR_JAPANTEST",
                    "WES_SNR_NavigateToMenu_CreateForecast"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            string[] tagsOfScenario = @__tags;
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Navigate to the Japan WES portal menus", null, @__tags);
#line 18
this.ScenarioInitialize(scenarioInfo);
#line hidden
            bool isScenarioIgnored = default(bool);
            bool isFeatureIgnored = default(bool);
            if ((tagsOfScenario != null))
            {
                isScenarioIgnored = tagsOfScenario.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((this._featureTags != null))
            {
                isFeatureIgnored = this._featureTags.Where(__entry => __entry != null).Where(__entry => String.Equals(__entry, "ignore", StringComparison.CurrentCultureIgnoreCase)).Any();
            }
            if ((isScenarioIgnored || isFeatureIgnored))
            {
                testRunner.SkipScenario();
            }
            else
            {
                this.ScenarioStart();
#line 19
 testRunner.Given(string.Format("I hover to {0}", menuItem), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
#line 20
 testRunner.When(string.Format("I click on the dropdown {0}", menuOption), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line hidden
#line 21
 testRunner.Then(string.Format("{0} should be dispayed on page", pageTitle), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            }
            this.ScenarioCleanup();
        }
        
        [TechTalk.SpecRun.ScenarioAttribute("Navigate to the Japan WES portal menus, FORECAST", new string[] {
                "WES_SNR_NavigateToMenu",
                "WES_SNR_JAPANTEST",
                "WES_SNR_NavigateToMenu_CreateForecast"}, SourceLine=24)]
        public virtual void NavigateToTheJapanWESPortalMenus_FORECAST()
        {
#line 18
this.NavigateToTheJapanWESPortalMenus("FORECAST", "Create Forecast", "CREATE FORECAST", ((string[])(null)));
#line hidden
        }
        
        [TechTalk.SpecRun.TestRunCleanup()]
        public virtual void TestRunCleanup()
        {
            TechTalk.SpecFlow.TestRunnerManager.GetTestRunner().OnTestRunEnd();
        }
    }
}
#pragma warning restore
#endregion
