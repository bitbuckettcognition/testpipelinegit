﻿@WES_FTR_NegativeTest_Deal_Entry_Upload_48_Interval_File @WES_FTR_JAPANTEST
Feature: WES_NegativeTest_Deal_Entry_Upload_48_Interval_File
	In order to test negative test
	I want to upload Supply_Upload_Template_Sample JE.xlsx to DEAL ENTRY - UPLOAD 48 INTERVAL FILE

@WES_SNR_NegativeTest_Deal_Entry_Upload_48_Interval_File @WES_SNR_JAPANTEST
Scenario Outline: Negative test for Deal Entry - Upload 48 Interval File
  Given I login to the WES portal
    And I redirect to the Deal Entry - Upload 48 Interval File
    And I choose the sample excel file to upload
   When I press click on upload
   Then The <Message> should be displayed on screen


	Examples: 
|                                     Message                                     |
|Invalid data found in row 3. Child Group ID is invalid. This file was not loaded.|
 