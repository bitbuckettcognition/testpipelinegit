﻿@WES_FTR_DataDependentTestCase_CreateForecast  @WES_FTR_USTEST
Feature: WES_DataDependentTestCase_CreateForecast
	In order to run Data Dependent Test Case
	Im export data from create forecast page and 
	Check the excel entries.

@WES_SNR_DataDependentTestCase_CreateForecast @WES_SNR_USTEST
Scenario Outline: Data Dependent Test Case for create forecast
  Given I fetch the forecast data from database for <Date>
  Given I login to the WES portal
   When I redirect to the create forecasts page
    And Enter the search fields using by database result and checks the result
	 
	 Examples: 
| Date       |
| 29-01-2019 |

	 
