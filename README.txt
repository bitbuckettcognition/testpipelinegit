This repository contains a visual studio unit test project with SpecFlow and Seleneum Webdriver using c# .net core.
This project is constructed to perform the automation testing on WES portal.


Important: Below are initial settings recommended to update "appsetting.json" in  route folder before runnin this source code.

1)In the appsetting.json file which contains keys for source paths.
  locate these path and replace with your local path (Added "ScreenCaptures" in .gitignore, Please create blank folder)
  EX. "ScreenCaptureFolderPath" :"<YOUR_PROJECT_DIRCTORY>\\WES.Test.Automation\\ScreenCaptures\\"  (Added "ScreenCaptures" in .gitignore, Please create blank folder)
  where YOUR_PROJECT_DIRCTORY is the root path of your .NET project.
  
2) ESG Portal login details: 
   To be able to automate login process in this project you might want to use your login details.
   To do so please check following keys.
	EX. "TestESGPortalUsername": "WES_PORTAL_USERNAME",
		"TestESGPortalPassword": "WES_PORTAL_PASSWORD", 

    Note: Update credentials for both US and Japan, test and prod
		
3) We have one test feature for the password recovery. for that we have to receive any reset password link 
   email in our inbox. We need to provide our register email credentials in keys as below. This way the automation will be 
   able to access your email and perform further process.
   
   EX: "TestEmailAddress": "YOUR_GMAIL_EMAIL_ADDRESS",
       "TestEmailPassword": "YOUR_GMAIL_PASSWORD", 
	
4) Running Headless Browser: This setting is useful when you wish to run the automation process in background, without having to open browser instances.
   to enable this one should update the initial call to the class WebDriverFactory.cs with CreateWebDriver function having 	"runHeadless" = true
   
   By default this is set to false, So you will see the browsers running while performing automation test.

5)In the appsetting.json file contains DBConnectionString string for connect to the DB.
  locate this DBConnectionString and set the database credentials User ID, Password and Data Source
  EX: replace, <USER_ID> to db user id,
               <PASSWORD> to db password,
               <DATA_SOURCE> to db name,
			   
  Note: Update credentials for both US and Japan, test and prod

6)In the appsetting.json file contains TestFailReportToEmailAddress array to send failed test message/email 
  locate this TestFailReportToEmailAddress and set the Name and Email 
  EX: replace, <NAME> name of email user or slack channel,
               <Email> email address
  We can able to one or more email address in TestFailReportToEmailAddress array	

6)In the appsetting.json file contains TestFailReportFromEmailAddress array from which we have to send an email for failed test
  locate this TestFailReportFromEmailAddress and set the Name and Email 
  EX: replace, <NAME> name of email u,
               <Email> email address
  By default we put TestEmailAddress as mentioned in #3
  
7)THIS IS THE TEST COMMIT FROM THE LIVE BITBUCKET.
  