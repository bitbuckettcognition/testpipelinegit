﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-08 02:21:45 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:47
 */
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.DataSensitiveTest
{
    [Binding]
    public class DataSensitiveTestSteps :SpecflowBase
    {
        OracleDataUtility _objOracleData;
        DataTable _dtResultData = new DataTable();
        string _dataType = "";
        int _failedRecordCounter = 0;
        readonly IWebDriver _driver;

        public DataSensitiveTestSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver; 
        }

        [Given(@"Connect to the datatbase ORATESTDB")]
        public void GivenConnectToTheDatatbaseORATESTDB()
        {
            
                _objOracleData = new OracleDataUtility();
            
        }


        DateTime _dtGivenDate;
        string _currentDate;

        [Given(@"Execute the Sql query (.*) and (.*) to get the result set from (.*)")]
        public void GivenExecuteTheSqlQueryAndToGetTheResultSetFrom(string queryString, string TestDate, string dataType)
        {

           
                _dataType = dataType;
                _currentDate = TestDate;
                var TestStartDate = Regex.Replace(TestDate, @"[^0-9]", "");
                TestStartDate = "'" + TestStartDate + "'";
                var pattern = @"<TestStartDate>";
                var UpdatedQueryString = Regex.Replace(queryString, pattern, TestStartDate);


                _ = DateTime.TryParse(TestDate, out _dtGivenDate);
                _dtGivenDate = _dtGivenDate.AddDays(-35);

                var TestEndDate = Regex.Replace(_dtGivenDate.ToString("yyyy-MM-dd"), @"[^0-9]", "");

                var whereIdrDateCondition = "to_date('" + TestEndDate + "','YYYYMMDD')";
                pattern = @"<TestEndDate>";

                UpdatedQueryString = Regex.Replace(UpdatedQueryString, pattern, whereIdrDateCondition);

                Console.WriteLine("Sql Query: " + UpdatedQueryString);

                _dtResultData = _objOracleData.GetDataTableFromQueryString(UpdatedQueryString);

            
        }

        [When(@"Compare I compare the result set")]
        public void WhenCompareICompareTheResultSet()
        {
            
                if (_dtResultData.Rows.Count > 0)
                {
                    var ssAccountId = "";
                    foreach (DataRow row in _dtResultData.Rows)
                    {
                        if (ssAccountId != row["SS_ACCT_ID"].ToString())
                        {

                            ssAccountId = row["SS_ACCT_ID"].ToString();
                        }
                        else
                        {
                            continue;
                        }
                        if (_failedRecordCounter == 0)
                        {
                            Console.WriteLine("-----------------------------------------------------------------------");
                            Console.WriteLine("Failed Records in " + _dataType);

                        }
                        Console.WriteLine("-----------------------------------------------------------------------");
                        Console.WriteLine("SS_ACCT_ID: " + row["SS_ACCT_ID"]);
                        Console.WriteLine(_dataType + " REC_ID: " + row["REC_ID"]);

                        if (_dataType == "NONIDRKH")
                        {
                            Console.WriteLine("Observed SVC_END_DATE: " + row["SVC_END_DATE"]);
                        }
                        else
                        {
                            Console.WriteLine("Observed IDR_DATE: " + row["IDR_DATE"]);
                        }

                        Console.WriteLine("Expected End Date: " + _dtGivenDate.ToString("yyyy-MM-dd"));
                        Console.WriteLine("Test Date: " + _currentDate);

                        _failedRecordCounter++;


                    }

                    Console.WriteLine("Summary: " + _failedRecordCounter + " Records are found in " + _dataType);
                }
                else
                {
                    Console.WriteLine("Records not found in " + _dataType);
                }
 
        }

        [Then(@"the result should be displayed")]
      
        public void ThenTheResultShouldBeDisplayed()
        {
            
                if (_dtResultData.Rows.Count > 0)
                {
                    Console.WriteLine(_failedRecordCounter + " Records are failed in " + _dataType);
                }
                else
                {
                    Console.WriteLine("Records not found in " + _dataType);
                }

                //get feature/scenario details
                var featureFileDetails = new Dictionary<string, string>
                {
                    { "featureTitle", FeatureContext.Current.FeatureInfo.Title },
                    { "featureDescription", FeatureContext.Current.FeatureInfo.Description },
                    { "scenarioTitle", ScenarioContext.Current.ScenarioInfo.Title }
                };

                var excelUtility = new ExcelUtility();
                var exelReport = excelUtility.CreateExcelReport(  "DataSensitiveTest_CreateForecasts_for_" + _dataType, "WES Portal - Data Sensitive Test Case - Create Forecast page for " + _dataType, _dtResultData, featureFileDetails);

                var commonStep = new CommonSteps(_driver);
                var config = commonStep.GetAppSettings();
                var reportFilepath = config["TestResultFolderPath"] + exelReport;
                 
                Console.WriteLine(reportFilepath);
             
        }

    }
}
