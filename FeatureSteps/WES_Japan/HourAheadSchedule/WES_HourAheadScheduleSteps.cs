﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-23 02:22:47 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:58
 */
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.WES_Japan.HourAheadSchedule
{
    [Binding]
    public class WES_HourAheadScheduleSteps : SpecflowBase
    {
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_HourAheadScheduleSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }


        [Given(@"I redirect to the Hourly Ahead Schedule")]
       
        public void GivenIRedirectToTheHourlyAheadSchedule()
        {

            var commonSteps = new CommonSteps(_driver);
            commonSteps.PerformMenuNavigation("SCHEDULES", "Hour Ahead Schedule");

        }

        [Then(@"The results grid should be displyed on the page")]
        public void ThenTheResultsGridShouldBeDisplyedOnThePage()
        {

            //check if grid is exists
            var isElementDisplayed = _driver.FindElement(By.Id("MarketHeaderInfoGrid")).Displayed;
            if (isElementDisplayed)
            {
                Console.WriteLine("Result grid is displayed");
                //check the records are present in the grid
                var recordXPath = "//*[@id='MarketHeaderInfoGrid']/table/tbody/tr[1]/td[1]/table/tbody[2]/tr/td/div[2]/table/tbody/tr[1]";
                var isRecordElementDisplayed = _driver.FindElement(By.XPath(recordXPath)).Displayed;
                if (isRecordElementDisplayed)
                {
                    Console.WriteLine("Record is present in the grid");
                    var pageScreenshotFilePath = _captureScreen.CapturePage("GridRecordError", _driver);
                    var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("GridResultError", By.Id("MarketHeaderInfoGrid"), _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));
                    Console.WriteLine(new Uri(elementScreenshotFilePath));
                }
                else
                {

                    var pageScreenshotFilePath = _captureScreen.CapturePage("GridRecordError", _driver);
                    var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("GridResultError", By.Id("MarketHeaderInfoGrid"), _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));
                    Console.WriteLine(new Uri(elementScreenshotFilePath));
                    Assert.Fail();

                }
            }
            else
            {

                var pageScreenshotFilePath = _captureScreen.CapturePage("resultGridError", _driver);
                Console.WriteLine("Result grid is not displyed");
                Console.WriteLine(new Uri(pageScreenshotFilePath));


                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Result grid is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }
    }
}
