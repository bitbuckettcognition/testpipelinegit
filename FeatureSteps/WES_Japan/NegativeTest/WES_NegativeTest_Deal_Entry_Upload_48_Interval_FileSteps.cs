﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-16 02:23:18 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:20
 */
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.NegativeTest
{
    [Binding]
    public class WES_NegativeTest_Deal_Entry_Upload_48_Interval_FileSteps : SpecflowBase
    {
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_NegativeTest_Deal_Entry_Upload_48_Interval_FileSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }

        [Given(@"I redirect to the Deal Entry - Upload (.*) Interval File")]
      
        public void GivenIRedirectToTheDealEntry_UploadIntervalFile(int p0)
        {

            var commonSteps = new CommonSteps(_driver);
            commonSteps.PerformMenuNavigation("PORTFOLIO MANAGEMENT", "Deal Entry - Upload 48 Interval File");

        }

        [Given(@"I choose the sample excel file to upload")]
        public void GivenIChooseTheSampleExcelFileToUpload()
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            //Locate the Web Elements
            var isElementDisplayed = _driver.FindElement(By.Id("FileUpload1")).Displayed;
            if (isElementDisplayed)
            {
                var commonStep = new CommonSteps(_driver);
                var config = commonStep.GetAppSettings();

                var sampleExcelfilePath = config["SampleUploadFilesFolderPath"] + "Supply_Upload_Template_Sample JE.xlsx";

                _driver.FindElement(By.Id("FileUpload1")).SendKeys(sampleExcelfilePath);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("FileUploadError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " File upload field is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [When(@"I press click on upload")]
        public void WhenIPressClickOnUpload()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webImageBtnSearch222")).Displayed;
            if (isElementDisplayed)
            {
                var uploadBtn = _driver.FindElement(By.Id("webImageBtnSearch222"));
                uploadBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UploadButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Upload button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [Then(@"The (.*) should be displayed on screen")]
        public void ThenTheShouldBeDisplayedOnScreen(string message)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("Label1")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("Label1")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("DealEntryUploadPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("DealEntryUploadMessage", By.Id("Label1"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("MessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Label message is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }
    }
}
