﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-11 02:21:14 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:43
 */
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.DataDependentTestCase_CreateForecast
{
    [Binding]
    public class WES_DataDependentTestCase_CreateForecastSteps : SpecflowBase
    {
        DataTable _dtResultData = new DataTable();
        DateTime _enteredDate;
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_DataDependentTestCase_CreateForecastSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }


        [Given(@"I fetch the forecast data from database for (.*)")]
        public void GivenIFetchTheForecastDataFromDatabaseFor(string Date)
        {
             
            _enteredDate = DateTime.Parse(Date);
            var forecastDate = _enteredDate.AddDays(10).ToString("dd-MMMM-yyyy");

            var startDate = DateTime.Parse(forecastDate);
            var endDate = DateTime.Parse(forecastDate);
            _ = startDate.ToString("yyyyMMdd");
            _ = endDate.ToString("yyyyMMdd");

            var queryString = "SELECT w.OPPORTUNITY_ID,w.ISO_CTRL_AREA,w.LDC_NAME,w.CONGESTION_ZONE,w.FORECAST_DATE," +
                " (w.kwh_0015 + w.kwh_0030 + w.kwh_0045 + w.kwh_0100 + w.kwh_0115 + w.kwh_0130 + w.kwh_0145 + w.kwh_0200 + w.kwh_0215 + w.kwh_0230 + w.kwh_0245 + w.kwh_0300 + w.kwh_0315 + w.kwh_0330 + w.kwh_0345 + w.kwh_0400 + w.kwh_0415 + w.kwh_0430 + w.kwh_0445 + w.kwh_0500 + w.kwh_0515 + w.kwh_0530 + w.kwh_0545 + w.kwh_0600 + w.kwh_0615 + w.kwh_0630 + w.kwh_0645 + w.kwh_0700 + w.kwh_0715 + w.kwh_0730 + w.kwh_0745 + w.kwh_0800 + w.kwh_0815 + w.kwh_0830 + w.kwh_0845 + w.kwh_0900 + w.kwh_0915 + w.kwh_0930 + w.kwh_0945 + w.kwh_1000 + w.kwh_1015 + w.kwh_1030 + w.kwh_1045 + w.kwh_1100 + w.kwh_1115 + w.kwh_1130 + w.kwh_1145 + w.kwh_1200 + w.kwh_1215 + w.kwh_1230 + w.kwh_1245 + w.kwh_1300 + w.kwh_1315 + w.kwh_1330 + w.kwh_1345 + w.kwh_1400 + w.kwh_1415 + w.kwh_1430 + w.kwh_1445 + w.kwh_1500 + w.kwh_1515 + w.kwh_1530 + w.kwh_1545 + w.kwh_1600 + w.kwh_1615 + w.kwh_1630 + w.kwh_1645 + w.kwh_1700 + w.kwh_1715 + w.kwh_1730 + w.kwh_1745 + w.kwh_1800 + w.kwh_1815 + w.kwh_1830 + w.kwh_1845 + w.kwh_1900 + w.kwh_1915 + w.kwh_1930 + w.kwh_1945 + w.kwh_2000 + w.kwh_2015 + w.kwh_2030 + w.kwh_2045 + w.kwh_2100 + w.kwh_2115 + w.kwh_2130 + w.kwh_2145 + w.kwh_2200 + w.kwh_2215 + w.kwh_2230 + w.kwh_2245 + w.kwh_2300 + w.kwh_2315 + w.kwh_2330 + w.kwh_2345 + w.kwh_2400) as DAILY_TOTAL" +
                " FROM ss.wesportal_forecast w, ss.iso_ldc_translation" +
                " WHERE" +
                " w.forecast_date = '" + forecastDate + "'" +
                " AND congestion_zone IS NOT NULL" +
                " AND w.ldc_duns = iso_ldc_translation.ldc_duns" +
                " AND ROWNUM <= 3" +
                " AND w.opportunity_id=69" +
                " ORDER BY ISO_CTRL_AREA asc";

            var objOracleData = new OracleDataUtility();
            _dtResultData = objOracleData.GetDataTableFromQueryString(queryString);


        }

        [When(@"Enter the search fields using by database result and checks the result")]
       
        public void WhenEnterTheSearchFieldsUsingByDatabaseResultAndChecksTheResult()
        {


            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            if (_dtResultData.Rows.Count > 0)
            {
                _ = _dtResultData.Columns.Add("RESULT_FOUND", typeof(string));
                _ = _dtResultData.Columns.Add("HOURLY_TOTAL", typeof(string));
                _ = _dtResultData.Columns.Add("TEST_RESULT", typeof(string));
                _ = _dtResultData.Columns.Add("COMMENT", typeof(string));
                _ = _dtResultData.Columns.Add("REFERENCE", typeof(string));
                var counter = 0;
                foreach (DataRow row in _dtResultData.Rows)
                {
                    var returnValue = PassFieldParameters(row);

                    if (returnValue)
                    {
                        var forecastDate = "";

                        //Locate the Forecast start date  Web Elements
                        //DateTime now = DateTime.Now;
                        forecastDate = _enteredDate.AddDays(10).ToString("M/d/yyyy");

                        var startDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("M/d/yyyy");

                        Thread.Sleep(1000);

                        var xpath = "//input[@value='" + startDate + "']";
                        var isStartDateElementDisplayed = _driver.FindElement(By.XPath(xpath)).Displayed;
                        if (isStartDateElementDisplayed)
                        {
                            _driver.FindElement(By.XPath(xpath)).Click();
                            _driver.FindElement(By.XPath(xpath)).SendKeys(forecastDate);
                        }
                        else
                        {
                            var pageScreenshotFilePath = _captureScreen.CapturePage("StartDateErrorError", _driver);
                            Console.WriteLine(new Uri(pageScreenshotFilePath));
                            Assert.Fail();
                        }

                        //Locate the Forecast end date  Web Elements 
                        var endDate = startDay.AddMonths(1).AddDays(-1).ToString("M/d/yyyy"); ;
                        Thread.Sleep(1000);
                        var xpathEndDate = "//input[@value='" + endDate + "']";
                        var isEndDateElementDisplayed = _driver.FindElement(By.XPath(xpathEndDate)).Displayed;
                        if (isEndDateElementDisplayed)
                        {
                            _driver.FindElement(By.XPath(xpathEndDate)).Click();
                            _driver.FindElement(By.XPath(xpathEndDate)).SendKeys(forecastDate);
                        }
                        else
                        {
                            var pageScreenshotFilePath = _captureScreen.CapturePage("EndDateErrorError", _driver);
                            Console.WriteLine(new Uri(pageScreenshotFilePath));
                            Assert.Fail();
                        }


                        counter++;
                        //click the search button
                        ClickTheSearchButton();
                        //check the result 
                        ThenOnlyOneRecordShouldBeDisplayedOnPage(row, forecastDate);
                    }
                    else
                    {
                        row["TEST_RESULT"] = "Fail";
                        row["RESULT_FOUND"] = "-";
                    }
                    var ResultScreen = _captureScreen.CapturePage("ResultErrorScreen", _driver);
                    row["REFERENCE"] = new Uri(ResultScreen);

                    var commonSteps = new CommonSteps(_driver);
                    commonSteps.PerformMenuNavigation("FORECASTS", "Create Forecast");

                }

                //get feature/scenario details
                //get feature/scenario details
                var featureFileDetails = new Dictionary<string, string>
                    {
                        { "featureTitle", FeatureContext.Current.FeatureInfo.Title },
                        { "featureDescription", FeatureContext.Current.FeatureInfo.Description },
                        { "scenarioTitle", ScenarioContext.Current.ScenarioInfo.Title }
                    };

                var excelUtility = new ExcelUtility();
                var exelReport = excelUtility.CreateExcelReport("DataDependentTestCase_CreateForecasts", "WES Portal - Automation Data Dependent Test Case - Create Forecast page ", _dtResultData, featureFileDetails);
                var commonStep = new CommonSteps(_driver);
                var config = commonStep.GetAppSettings();
                var reportFilepath = config["TestResultFolderPath"] + exelReport;

                Console.WriteLine(reportFilepath);
            }
            else
            {
                Console.WriteLine("Records not found");

                //.........Send email or slack message for failed test starts
                

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Records not found in table\n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }


        public bool PassFieldParameters(DataRow row)
        {
            var ISO_CTRL_AREA = row["ISO_CTRL_AREA"].ToString();
            var LDC_NAME = row["LDC_NAME"].ToString();
            var CONGESTION_ZONE = row["CONGESTION_ZONE"].ToString();

            Thread.Sleep(2000);
            //Locate the ISO Web Elements
            var isElementDisplayed = _driver.FindElement(By.Id("dropDownIso")).Displayed;
            if (isElementDisplayed)
            {
                // select the drop down list
                IWebElement select = _driver.FindElement(By.Id("dropDownIso"));
                //create select element object 
                var selectElement = new SelectElement(select);


                var isOptionPresent = false;
                IList<IWebElement> allOptions = selectElement.Options;


                for (var i = 0; i < allOptions.Count; i++)
                {
                    if (allOptions[i].Text.Equals(ISO_CTRL_AREA))
                    {
                        isOptionPresent = true;
                        break;
                    }
                }
                if (isOptionPresent)
                {
                    //select by value
                    selectElement.SelectByText(ISO_CTRL_AREA);
                }
                else
                {
                    select.Click();
                    var pageScreenshotFilePath = _captureScreen.CapturePage("ISOOptionsError", _driver);
                    Console.WriteLine(ISO_CTRL_AREA + " Option missing in ISO dropdown");
                    Console.WriteLine(new Uri(pageScreenshotFilePath));

                    row["COMMENT"] = ISO_CTRL_AREA + " Option missing in ISO dropdown";
                    return false;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ISOError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " ISO dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

            Thread.Sleep(2000);
            //Locate the Utility Web Elements
            var isUtilityElementDisplayed = _driver.FindElement(By.Id("dropDownUtility")).Displayed;
            if (isUtilityElementDisplayed)
            {
                // select the drop down list
                var select = _driver.FindElement(By.Id("dropDownUtility"));
                //create select element object 
                var selectElement = new SelectElement(select);

                var isOptionPresent = false;
                IList<IWebElement> allOptions = selectElement.Options;
                for (var i = 0; i < allOptions.Count; i++)
                {
                    if (allOptions[i].Text.Equals(LDC_NAME))
                    {
                        isOptionPresent = true;
                        break;
                    }
                }
                if (isOptionPresent)
                {
                    //select by value
                    selectElement.SelectByText(LDC_NAME);
                }
                else
                {
                    select.Click();
                    var pageScreenshotFilePath = _captureScreen.CapturePage("UtilityOptionsError", _driver);
                    Console.WriteLine(LDC_NAME + " Option missing in Utility dropdown");
                    Console.WriteLine(new Uri(pageScreenshotFilePath));

                    row["COMMENT"] = LDC_NAME + " Option missing in Utility dropdown";
                    return false;
                }

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UtilityError", _driver);
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Utility dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Console.WriteLine(new Uri(pageScreenshotFilePath));
            }

            Thread.Sleep(2000);
            //Locate the Conjestion  Web Elements
            var isConjestionElementDisplayed = _driver.FindElement(By.Id("dropDownCongestionZone")).Displayed;
            if (isConjestionElementDisplayed)
            {
                // select the drop down list
                var select = _driver.FindElement(By.Id("dropDownCongestionZone"));
                //create select element object 
                var selectElement = new SelectElement(select);

                var isOptionPresent = false;
                IList<IWebElement> allOptions = selectElement.Options;
                for (var i = 0; i < allOptions.Count; i++)
                {
                    if (allOptions[i].Text.Equals(CONGESTION_ZONE))
                    {
                        isOptionPresent = true;
                        break;
                    }
                }
                if (isOptionPresent)
                {
                    //select by value
                    selectElement.SelectByText(CONGESTION_ZONE);
                }
                else
                {
                    select.Click();
                    var pageScreenshotFilePath = _captureScreen.CapturePage("UtilityOptionsError", _driver);
                    Console.WriteLine(CONGESTION_ZONE + " Option missing in Congestion zone dropdown");
                    Console.WriteLine(new Uri(pageScreenshotFilePath));

                    row["COMMENT"] = CONGESTION_ZONE + " Option missing in Congestion zone dropdown";
                    return false;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ConjestionError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Congestion dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
            return true;
        }


        public void ClickTheSearchButton()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webImageBtnSearch2")).Displayed;
            if (isElementDisplayed)
            {
                var submitBtn = _driver.FindElement(By.Id("webImageBtnSearch2"));
                submitBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("SubmitButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Search button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }


        public void ThenOnlyOneRecordShouldBeDisplayedOnPage(DataRow row, string forecastDate)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var xpathResult = "//*[@id='webDataGridResults']/table/tbody/tr[1]/td[1]/table/tbody[2]/tr/td/div[2]/table/tbody/tr";
            var isElementDisplayed = _driver.FindElement(By.XPath(xpathResult)).Displayed;
            if (isElementDisplayed)
            {
                IList<IWebElement> selectResultElements = _driver.FindElements(By.XPath(xpathResult));
                row["RESULT_FOUND"] = selectResultElements.Count;
                if (selectResultElements.Count != 1)
                {
                    Console.WriteLine("-----------------------------------------------------------------------");
                    Console.WriteLine("------------------------------TEST FAILED------------------------------");
                    Console.WriteLine("-----------------------------------------------------------------------");
                    row["TEST_RESULT"] = "Fail";
                    row["COMMENT"] = "More than 1 result found";

                }
                else
                {
                    var originalDriverWidth = _driver.Manage().Window.Size.Width;
                    var originalDriverHeight = _driver.Manage().Window.Size.Height;

                    _driver.Manage().Window.Size = new System.Drawing.Size(4080, 1080);

                    var xpathForHour = "//*[@id='webDataGridResults']/table/tbody/tr[1]/td[1]/table/tbody[2]/tr/td/div[2]/table/tbody/tr/td[7]/div/table/tbody/tr/td";

                    double hourlyTotal = 0;
                    for (var i = 1; i <= 24; i++)
                    {
                        var elementXPath = xpathForHour + "[" + i + "]";
                        var isHourlyElementDisplayed = _driver.FindElement(By.XPath(elementXPath)).Displayed;
                        if (isHourlyElementDisplayed)
                        {
                            var singleHourValue = _driver.FindElement(By.XPath(elementXPath)).Text;
                            hourlyTotal += Convert.ToDouble(singleHourValue);
                        }
                        else
                        {
                            Console.WriteLine("Hourly value element not found");
                        }
                    }

                    _driver.Manage().Window.Size = new System.Drawing.Size(originalDriverWidth, originalDriverHeight);

                    hourlyTotal = Math.Round(hourlyTotal, 2);

                    row["HOURLY_TOTAL"] = hourlyTotal;

                    if (Math.Round(Convert.ToDouble(row["DAILY_TOTAL"]), 2) != hourlyTotal)
                    {

                        Console.WriteLine("-----------------------------------------------------------------------");
                        Console.WriteLine("------------------------------TEST FAILED------------------------------");
                        Console.WriteLine("-----------------------------------------------------------------------");
                        row["TEST_RESULT"] = "Fail";
                        row["COMMENT"] = "DAILY_TOTAL not matching with HOURLY_TOTAL";

                    }
                    else
                    {

                        Console.WriteLine("-----------------------------------------------------------------------");
                        Console.WriteLine("------------------------------TEST PASSED------------------------------");
                        Console.WriteLine("-----------------------------------------------------------------------");

                        row["TEST_RESULT"] = "Pass";

                    }
                }

                Console.WriteLine("OPPORTUNITY_ID: " + row["OPPORTUNITY_ID"]);
                Console.WriteLine("ISO: " + row["ISO_CTRL_AREA"]);
                Console.WriteLine("UTILITY: " + row["LDC_NAME"]);
                Console.WriteLine("CONGESTION ZONE: " + row["CONGESTION_ZONE"]);
                Console.WriteLine("START DATE: " + forecastDate);
                Console.WriteLine("END DATE: " + forecastDate);

                var pageScreenshotFilePath = _captureScreen.CapturePage("ResultErrorScreen", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("RecordError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine("Result not found");

            } 
        }
    }
}
