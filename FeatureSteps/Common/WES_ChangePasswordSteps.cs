﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-09 20:27:09 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:25
 */

using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.Common
{
    [Binding]
    public class WES_ChangePasswordSteps : SpecflowBase
    {

        private bool _isOldPasswordValid;
        private bool _isNewPasswordValid;
        private bool _isConfirmPasswordValid;
        private string _tempPasswordHolder = null;
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();

        public WES_ChangePasswordSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        [BeforeScenario("WES_SNR_ChangePassword")]
       
        public void BeforeScenario()
        {
            var commonSteps = new CommonSteps(_driver);
            commonSteps.PerformLogin();
            commonSteps.PerformMenuNavigation("HELP", "Change Password");
        }

        /// <summary>
        /// method for invalid old password
        /// </summary>
        /// <param name="oldPassword"></param>

        [Given(@"I enter (.*)")]
        
        public void GivenIEnter(string oldPassword)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextCPwd")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var oldPasswordBox = _driver.FindElement(By.Id("webTextCPwd"));

                //Perform Required action with the element
                oldPasswordBox.Click();
                oldPasswordBox.SendKeys(oldPassword);
                var validatePassword = new ValidatePassword();
                _isOldPasswordValid = validatePassword.CheckPasswordPattern(oldPassword);

                var commonStep = new CommonSteps(_driver);
                var config = commonStep.GetAppSettings();
                 
                var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
               
                var password = "";
                if (currentTags.Contains("WES_SNR_USTEST"))
                { 
                    password = config["TestESGPortalPassword_USTest"];
                }
                else if (currentTags.Contains("WES_SNR_JAPANTEST"))
                { 
                    password = config["TestESGPortalPassword_JPTest"]; 
                }
                else if (currentTags.Contains("WES_SNR_USPROD"))
                { 
                    password = config["TestESGPortalPassword_USProd"];
                }
                else if (currentTags.Contains("WES_SNR_JAPANPROD"))
                { 
                    password = config["TestESGPortalPassword_JPProd"];
                }


                if (password != oldPassword)
                {
                    _isOldPasswordValid = false;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("OldPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Old password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        /// <summary>
        /// method for a valid old password
        /// </summary>

        [Given(@"I have enter valid old password")]
        
        public void GivenIHaveEnterValidOldPassword()
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextCPwd")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var oldPasswordBox = _driver.FindElement(By.Id("webTextCPwd"));

                //Perform Required action with the element
                oldPasswordBox.Click();

                var commonStep = new CommonSteps(_driver);
                var config = commonStep.GetAppSettings();


                var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
               
                var password = "";
                if (currentTags.Contains("WES_SNR_USTEST"))
                {
                    
                    password = config["TestESGPortalPassword_USTest"];
                }
                else if (currentTags.Contains("WES_SNR_JAPANTEST"))
                {
                    
                    password = config["TestESGPortalPassword_JPTest"];

                }
                else if (currentTags.Contains("WES_SNR_USPROD"))
                {
                    
                    password = config["TestESGPortalPassword_USProd"];
                }
                else if (currentTags.Contains("WES_SNR_JAPANPROD"))
                {
                     
                    password = config["TestESGPortalPassword_JPProd"];
                }

                oldPasswordBox.SendKeys(password);
                var validatePassword = new ValidatePassword();
                _isOldPasswordValid = validatePassword.CheckPasswordPattern(password);

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("OldPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Old password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }


        [Given(@"I entered the (.*) field")]
       
        public void GivenIEnteredTheField(string newPassword)
        {


            var isElementDisplayed = _driver.FindElement(By.Id("webTextNewPwd1")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var newPasswordBox = _driver.FindElement(By.Id("webTextNewPwd1"));

                //Perform Required action with the element
                newPasswordBox.Click();
                newPasswordBox.SendKeys(newPassword);
                var validatePassword = new ValidatePassword();
                _isNewPasswordValid = validatePassword.CheckPasswordPattern(newPassword);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("NewPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " New password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }

        [Given(@"I entered the (.*) field to confirm")]
        
        public void GivenIEnteredTheFieldToConfirm(string confirmPassword)
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webTextNewPwd2")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var confirmPasswordBox = _driver.FindElement(By.Id("webTextNewPwd2"));

                //Perform Required action with the element
                confirmPasswordBox.Click();
                confirmPasswordBox.SendKeys(confirmPassword);
                var validatePassword = new ValidatePassword();
                _isConfirmPasswordValid = validatePassword.CheckPasswordPattern(confirmPassword);
                //set temp password to update when changed successfully
                if (_isOldPasswordValid && _isNewPasswordValid && _isConfirmPasswordValid)
                {
                    _tempPasswordHolder = confirmPassword;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ConfirmPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Confirm password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }

        }


        [When(@"I press change password button")]
        
        public void WhenIPressChangePasswordButton()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webImageBtnLogin")).Displayed;
            if (isElementDisplayed)
            {
                var submitBtn = _driver.FindElement(By.Id("webImageBtnLogin"));
                submitBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("SubmitButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Submit button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }
        }


        [Then(@"The (.*) should be display on the page")]
      
        public void ThenTheShouldBeDisplayOnThePage(string message)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("LabelMessage")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("LabelMessage")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("ChangePasswordPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LabelMessage", By.Id("LabelMessage"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));
                 
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LabelMessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Message label is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }
        }


        [Then(@"Success (.*) should be display on the page")]
        public void ThenSuccessShouldBeDisplayOnThePage(string message)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("LabelMessage")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("LabelMessage")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("ChangePasswordPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LabelMessage", By.Id("LabelMessage"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));

                if (actualMessage == message && _tempPasswordHolder != null)
                {

                    var objCommonSteps = new CommonSteps(_driver);
                    objCommonSteps.UpdatePasswordInConfig(_tempPasswordHolder);
                    _tempPasswordHolder = null;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LabelMessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Message label is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }
        }



    }
}
