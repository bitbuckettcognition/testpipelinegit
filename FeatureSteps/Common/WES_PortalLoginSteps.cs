﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-04 20:25:38 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:34
 */

using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.Common
{
    [Binding]
    public class WES_PortalLoginSteps : SpecflowBase
    {
        private bool _isPasswordValid;
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_PortalLoginSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        [Given(@"I navigate to the WES portal (.*)")]
        public void GivenINavigateToTheWESPortal(string Url)
        {
           
            _driver.Navigate().GoToUrl(Url);
        } 

        [Given(@"Enter the valid credentials")]
       
        public void GivenEnterTheValidCredentials()
        {

            //get username from appsetting
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags; 
            var username = "";
            var password = "";
            if (currentTags.Contains("WES_SNR_USTEST"))
            { 
                username = config["TestESGPortalUsername_USTest"];
                password = config["TestESGPortalPassword_USTest"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            { 
                username = config["TestESGPortalUsername_JPTest"];
                password = config["TestESGPortalPassword_JPTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            { 
                username = config["TestESGPortalUsername_USProd"];
                password = config["TestESGPortalPassword_USProd"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            { 
                username = config["TestESGPortalUsername_JPProd"];
                password = config["TestESGPortalPassword_JPProd"];
            }
             

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                var passwordBox = _driver.FindElement(By.Id("webTextPwd"));

                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);

                passwordBox.Click();
                passwordBox.SendKeys(password);
                var validatePassword = new ValidatePassword();
                _isPasswordValid = validatePassword.CheckPasswordPattern(password);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            } 
        }


        [Given(@"I login to the WES portal with valid username and invalid (.*) on login page")]
        
        public void GivenILoginToTheWESPortalWithValidUsernameAndInvalidOnLoginPage(string password)
        {

            //get username from appsetting
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
            var username = "";

            if (currentTags.Contains("WES_SNR_USTEST"))
            {
                username = config["TestESGPortalUsername_USTest"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
                username = config["TestESGPortalUsername_JPTest"];
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
                username = config["TestESGPortalUsername_USProd"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            {
                username = config["TestESGPortalUsername_JPProd"];
            }

            

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                var passwordBox = _driver.FindElement(By.Id("webTextPwd"));

                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);

                passwordBox.Click();
                passwordBox.SendKeys(password);
                var validatePassword = new ValidatePassword();
                _isPasswordValid = validatePassword.CheckPasswordPattern(password);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();

            }
        }


        [Given(@"I login to the WES portal with Invalid (.*) and valid password on login page")]
       
        public void GivenILoginToTheWESPortalWithInvalidAndValidPasswordOnLoginPage(string username)
        {

            //get username from appsetting

            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags; 
            var password = "";
            if (currentTags.Contains("WES_SNR_USTEST"))
            { 
                password = config["TestESGPortalPassword_USTest"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            { 
                password = config["TestESGPortalPassword_JPTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            { 
                password = config["TestESGPortalPassword_USProd"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            { 
                password = config["TestESGPortalPassword_JPProd"];
            }

           

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                var passwordBox = _driver.FindElement(By.Id("webTextPwd"));

                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);

                passwordBox.Click();
                passwordBox.SendKeys(password);
                var validatePassword = new ValidatePassword();
                _isPasswordValid = validatePassword.CheckPasswordPattern(password);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();

            }
        }


        [Given(@"I login to the WES with (.*) and (.*) on login page")]
        
        public void GivenILoginToTheWESWithAndOnLoginPage(string username, string password)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                var passwordBox = _driver.FindElement(By.Id("webTextPwd"));

                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);

                passwordBox.Click();
                passwordBox.SendKeys(password);
                var validatePassword = new ValidatePassword();
                _isPasswordValid = validatePassword.CheckPasswordPattern(password);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }

        [When(@"I clicked on the login button")]
       
        public void WhenIClickedOnTheLoginButton()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webImageBtnLogin")).Displayed;
            if (isElementDisplayed)
            {
                var submitBtn = _driver.FindElement(By.Id("webImageBtnLogin"));
                submitBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("SubmitButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Login button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }

        [Then(@"Profile Name should be dispayed on the home page")]
        
        public void ThenProfileNameShouldBeDispayedOnTheHomePage()
        {

            //get profile name from appsetting
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags; 
            var expectedUser = "";
            if (currentTags.Contains("WES_SNR_USTEST"))
            {
                expectedUser = config["TestESGPortalProfileName_USTest"].ToString(); 
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
                expectedUser = config["TestESGPortalProfileName_JPTest"].ToString(); 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
                expectedUser = config["TestESGPortalProfileName_USProd"].ToString();  
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            {
                expectedUser = config["TestESGPortalProfileName_JPProd"].ToString();  
            }

             

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            if (_isPasswordValid)
            {
                var isElementDisplayed = _driver.FindElement(By.Id("labelUserInfo")).Displayed;
                if (isElementDisplayed)
                {
                    var actualUser = _driver.FindElement(By.Id("labelUserInfo")).Text;
                    Assert.IsTrue(actualUser.Contains(expectedUser));

                    var pageScreenshotFilePath = _captureScreen.CapturePage("LoggedInUserInfo", _driver);
                    var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LoggedInUserInfoElement", By.Id("labelUserInfo"), _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));
                    Console.WriteLine(new Uri(elementScreenshotFilePath));
                }
                else
                {
                    var pageScreenshotFilePath = _captureScreen.CapturePage("LoggedInUserInfo", _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath)); 

                    //.........Send email or slack message for failed test starts
                    var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                    var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                    var content = "User profile name is not displayed on the page \n";
                    content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                    var mailUtility = new MailUtility();
                    mailUtility.SendEmailMessage(subject, content, attachment);
                    //........end email or slack message for failed test ends

                    Assert.Fail();
                }
            }
            else
            {
                var isErrorDisplayed = _driver.FindElement(By.Id("labelLoginErr")).Displayed;
                if (isErrorDisplayed)
                {

                    var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                    var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LoginErrorMessage", By.Id("labelLoginErr"), _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));
                    Console.WriteLine(new Uri(elementScreenshotFilePath));
                }
                else
                {
                    var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));

                    //.........Send email or slack message for failed test starts
                    var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                    var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                    var content = "Error message is not displayed on the page \n";
                    content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                    var mailUtility = new MailUtility();
                    mailUtility.SendEmailMessage(subject, content, attachment);
                    //........end email or slack message for failed test ends
                    Assert.Fail();
                }
            }
        }

        [Then(@"The (.*) should be dispayed on the login page")]
       
        public void ThenTheShouldBeDispayedOnTheLoginPage(string errorMessage)
        {

            var isErrorDisplayed = _driver.FindElement(By.Id("labelLoginErr")).Displayed;
            if (isErrorDisplayed)
            {
                var actualErrorMessage = _driver.FindElement(By.Id("labelLoginErr")).Text;
                Assert.AreEqual(actualErrorMessage, errorMessage);
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LoginErrorMessage", By.Id("labelLoginErr"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LoginError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = errorMessage + " message is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }
    }
}
