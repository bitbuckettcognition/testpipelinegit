﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-01 20:27:37 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:30
 */

using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.Common
{
    [Binding]
    public class WES_NavigateToMenuSteps : SpecflowBase
    {
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();


        public WES_NavigateToMenuSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        [BeforeScenario("WES_SNR_NavigateToMenu")]
        
        public void BeforeScenario()
        { 
            var commonSteps = new CommonSteps(_driver);
            commonSteps.PerformLogin(); 
        }

        [Given(@"I hover to (.*)")]
        
        public void GivenIHoverTo(string menuItem)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.ClassName("igdm_Office2007BlueMenuItemHorizontalRootLink")).Displayed;
            if (isElementDisplayed)
            {
                IList<IWebElement> allMenuLinks = _driver.FindElements(By.ClassName("igdm_Office2007BlueMenuItemHorizontalRootLink"));
                var isMenuPresent = false;
                for (var i = 0; i < allMenuLinks.Count; i++)
                {
                    var IsItem = allMenuLinks.ElementAt(i);
                    if (IsItem.Text == menuItem)
                    {
                        var action = new Actions(_driver);
                        action.MoveToElement(IsItem).Perform();
                        isMenuPresent = true;
                        break;
                    }
                }
                if (isMenuPresent == false)
                {
                    var pageScreenshotFilePath = _captureScreen.CapturePage("ViewPageMenu", _driver);
                    Console.WriteLine(new Uri(pageScreenshotFilePath));
                    //.........Send email or slack message for failed test starts
                    var attachment = new List<FileInfo>
                    {
                        new FileInfo(pageScreenshotFilePath)
                    };

                    var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                    var content = menuItem + " option not present in menu \n";
                    content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                    var mailUtility = new MailUtility();
                    mailUtility.SendEmailMessage(subject, content, attachment);
                    //........end email or slack message for failed test ends
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ViewPageMenu", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = menuItem + " option not present in menu \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }

        [When(@"I click on the dropdown (.*)")]
        
        public void WhenIClickOnTheDropdown(string menuOption)
        {
            var isElementDisplayed = _driver.FindElement(By.XPath("//span[text()='" + menuOption + "']")).Displayed;
            if (isElementDisplayed)
            {
                _driver.FindElement(By.XPath("//span[text()='" + menuOption + "']")).Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage(menuOption + " MenuOptionError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = menuOption + " option not present in menu \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }

        [Then(@"(.*) should be dispayed on page")]
       
        public void ThenShouldBeDispayedOnPage(string pageTitle)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.ClassName("pageTitle")).Displayed;
            if (isElementDisplayed)
            {
                var actualUser = _driver.FindElement(By.ClassName("pageTitle")).Text;
                Assert.AreEqual(actualUser, pageTitle);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage(pageTitle + " PageTitleError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = pageTitle + " page title is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }
    }
}
