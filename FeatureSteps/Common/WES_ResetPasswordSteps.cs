﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-09 20:26:38 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:39
 */

 
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using TechTalk.SpecFlow;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.Common
{
    [Binding]
    public class WES_ResetPasswordSteps : SpecflowBase
    {

        private bool _isNewPasswordValid;
        private bool _isConfirmPasswordValid;
        private string _tempPasswordHolder = null;
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_ResetPasswordSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }

        //.....................

        [Given(@"I redirect to the WES portal")]
        public void GivenIRedirectToTheWESPortal()
        {
             
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
            //----------------------------------validate last execution date 
            if (currentTags.Contains("WES_SNR_ResetPassword"))
            {
                var resetPasswordIntervalTime = config["ResetPasswordIntervalTime"];
                var currentDate = DateTime.Now.ToString("ddMMyyyyhhmmss");

                var oldDateTimeSpan = DateTime.ParseExact(resetPasswordIntervalTime, "ddMMyyyyhhmmss", CultureInfo.InvariantCulture);
                var newDateTimeSpan = DateTime.ParseExact(currentDate, "ddMMyyyyhhmmss", CultureInfo.InvariantCulture);

                var ResetPasswordTimeSpan = config["ResetPasswordTimeSpan"];
                var ResetPasswordTimeSpanHour = Convert.ToDouble(ResetPasswordTimeSpan) / 60;

                if (resetPasswordIntervalTime != null)
                {
                    var timeSpanDiff = (newDateTimeSpan - oldDateTimeSpan).TotalHours;
                    if (timeSpanDiff <= ResetPasswordTimeSpanHour)
                    {
                        Console.WriteLine("Test must be execute after 1 hr interval. Last execution time: " + oldDateTimeSpan.ToString("dd-MM-yyyy hh:mm:ss"));
                        Assert.Fail();
                    }
                    else
                    {
                        var configFilePath = config["AppConfigFilePath"];
                        var text = File.ReadAllText(configFilePath);
                        var textToReplace = oldDateTimeSpan.ToString("ddMMyyyyhhmmss");
                        var updatedText = newDateTimeSpan.ToString("ddMMyyyyhhmmss");
                        text = text.Replace(textToReplace, updatedText);
                        File.WriteAllText(configFilePath, text);
                    }
                }
                else
                {
                    Console.WriteLine("ResetPasswordIntervalTime not present in appsetting.json");
                    Assert.Fail();
                }
            }
            //----------------------------------


            var redirectUrl = "";
             
            if (currentTags.Contains("WES_SNR_USTEST"))
            {
                redirectUrl = config["ESGPortalUrl_USTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
                redirectUrl = config["ESGPortalUrl_JPTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
                redirectUrl = config["ESGPortalUrl_USProd"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            {
                redirectUrl = config["ESGPortalUrl_JPProd"]; 
            } 
            _driver.Navigate().GoToUrl(redirectUrl); 
        }


        [Given(@"enter a valid username to get reset password link")]
        public void GivenEnterAValidUsernameToGetResetPasswordLink()
        {

            //get username from appsetting
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags; 
            var username = ""; 
            if (currentTags.Contains("WES_SNR_USTEST"))
            { 
                username = config["TestESGPortalUsername_USTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            { 
                username = config["TestESGPortalUsername_JPTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            { 
                username = config["TestESGPortalUsername_USProd"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            { 
                username = config["TestESGPortalUsername_JPProd"]; 
            }

            

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UsernameError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Username textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }



        [Given(@"enter an (.*) to get reset password link")]
        public void GivenEnterAnToGetResetPasswordLink(string username)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("webTextUid")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var usernameBox = _driver.FindElement(By.Id("webTextUid"));
                //Perform Required action with the element
                usernameBox.Click();
                usernameBox.SendKeys(username);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UsernameError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Username textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }

        }



        [Given(@"I click on the forgot my password link")]
        public void GivenIClickOnTheForgotMyPasswordLink()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("btnForgotPass")).Displayed;
            if (isElementDisplayed)
            {
                _driver.FindElement(By.Id("btnForgotPass")).Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UsernameError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Forgot password button/link is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }

        }


        [Then(@"The (.*) should be display on alert box")]
        public void ThenTheShouldBeDisplayOnAlertBox(string message)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var alertBox = _driver.SwitchTo().Alert();
            Assert.AreEqual(alertBox.Text, message);
            _driver.SwitchTo().Alert().Accept();

        }

        [Then(@"I open an email and click on the reset password link")]
        public void ThenIOpenAnEmailAndClickOnTheResetPasswordLink()
        {
            Thread.Sleep(4000);

            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var mailRepository = new MailRepository(config["TestEmailIMAPHost"], Convert.ToInt32(config["TestEmailIMAPPort"]),Convert.ToBoolean(config["TestEmailIMAPSSL"]), config["TestEmailAddress"], config["TestEmailPassword"]);
            var allEmails = mailRepository.GetAllMails();
            if (allEmails.Count() != 0)
            {
                string resetPasswordLink = null;
                var urlRex = new Regex(@"((https?|ftp|file|http)\://|www.)[A-Za-z0-9\.\-]+(/[A-Za-z0-9\?\&\=;\+!'\(\)\*\-\._~%]*)*", RegexOptions.IgnoreCase);

                foreach (var email in allEmails)
                {
                    MatchCollection matches = urlRex.Matches(email);
                    if (matches.Count > 0)
                    {
                        resetPasswordLink = matches[0].Value.ToString();
                    }
                }
                if (resetPasswordLink == null) {

                    //.........Send email or slack message for failed test starts
                     
                    var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                    var content = " Email not received for reset password link \n";
                    content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                    var mailUtility = new MailUtility();
                    mailUtility.SendEmailMessage(subject, content);
                    //........end email or slack message for failed test ends
                    Assert.Fail();

                }
                _driver.Navigate().GoToUrl(resetPasswordLink);
                Console.WriteLine(resetPasswordLink);

            }
            else
            {
                Console.WriteLine("Email not recieved");
                //.........Send email or slack message for failed test starts

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Email not received for reset password link \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }

        }

        //..........................



        [When(@"I entered the (.*) field on page")]
        public void WhenIEnteredTheFieldOnPage(string newPassword)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            var isElementDisplayed = _driver.FindElement(By.Id("webTextPwd")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var newPasswordBox = _driver.FindElement(By.Id("webTextPwd"));

                //Perform Required action with the element
                newPasswordBox.Click();
                newPasswordBox.SendKeys(newPassword);
                var validatePassword = new ValidatePassword();
                _isNewPasswordValid = validatePassword.CheckPasswordPattern(newPassword);

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("NewPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " New password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [When(@"I entered the (.*) field to confirm password")]
        public void WhenIEnteredTheFieldToConfirmPassword(string confirmPassword)
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webTextCPwd")).Displayed;
            if (isElementDisplayed)
            {
                //Locate the Web Elements
                var confirmPasswordBox = _driver.FindElement(By.Id("webTextCPwd"));

                //Perform Required action with the element
                confirmPasswordBox.Click();
                confirmPasswordBox.SendKeys(confirmPassword);
                var validatePassword = new ValidatePassword();
                _isConfirmPasswordValid = validatePassword.CheckPasswordPattern(confirmPassword);
                //set temp password to update when changed successfully
                if (_isNewPasswordValid && _isConfirmPasswordValid)
                {
                    _tempPasswordHolder = confirmPassword;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ConfirmPasswordFieldError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Confirm password textbox is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [When(@"I press reset password button")]
        public void WhenIPressResetPasswordButton()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("ButtonPassword")).Displayed;
            if (isElementDisplayed)
            {
                var submitBtn = _driver.FindElement(By.Id("ButtonPassword"));
                submitBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("SubmitButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Submit button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [Then(@"The (.*) should be display")]
        public void ThenTheShouldBeDisplay(string message)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            var isElementDisplayed = _driver.FindElement(By.Id("LabelError")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("LabelError")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("ChangePasswordPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LabelMessage", By.Id("LabelError"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));

                if (actualMessage == message && _tempPasswordHolder != null)
                { 
                    var objCommonSteps = new CommonSteps(_driver);
                    objCommonSteps.UpdatePasswordInConfig(_tempPasswordHolder);
                    _tempPasswordHolder = null;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LabelMessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Label message is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }

        }

        [Then(@"Success label (.*) should be display")]
        public void ThenSuccessLabelShouldBeDisplay(string message)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);

            var isElementDisplayed = _driver.FindElement(By.Id("LabelResetSuccess")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("LabelResetSuccess")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("ResetPasswordPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LabelMessage", By.Id("LabelResetSuccess"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));

                if (actualMessage == message && _tempPasswordHolder != null)
                {
                    var objCommonSteps = new CommonSteps(_driver);
                    objCommonSteps.UpdatePasswordInConfig(_tempPasswordHolder);
                    _tempPasswordHolder = null;
                }
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LabelMessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Label message is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends
                Assert.Fail();
            }
        }

    }
}
