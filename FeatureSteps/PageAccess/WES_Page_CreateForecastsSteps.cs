﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-08 02:22:26 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:43:52
 */

using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using TechTalk.SpecFlow;
using WES_TEST_Automation.FeatureSteps.Common;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.FeatureSteps.PageAccess
{
    [Binding]
    public class WES_Page_CreateForecastsSteps : SpecflowBase
    {
        private WES_PortalLoginSteps _wes_PortalLoginSteps;
        readonly IWebDriver _driver;
        readonly CaptureScreen _captureScreen = new CaptureScreen();
        public WES_Page_CreateForecastsSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }

         
        [Given(@"I login to the WES portal")]
       
        public void GivenILoginToTheWESPortal()
        {
            var _commonSteps = new CommonSteps(_driver);
            _wes_PortalLoginSteps = new WES_PortalLoginSteps(_driver);
            _commonSteps.PerformLogin();
        }

        [When(@"I redirect to the create forecasts page")]
       
        public void WhenIRedirectToTheCreateForecastsPage()
        {
            var _commonSteps = new CommonSteps(_driver);
            _commonSteps.PerformMenuNavigation("FORECASTS", "Create Forecast");
        }

        [When(@"Select (.*) ISO from dropdown")]
        public void WhenSelectISOFromDropdown(string IOSValue)
        {

            var isElementDisplayed = _driver.FindElement(By.Id("dropDownIso")).Displayed;
            if (isElementDisplayed)
            {
                // select the drop down list
                var selectIOS = _driver.FindElement(By.Id("dropDownIso"));
                //create select element object 
                var selectElement = new SelectElement(selectIOS);
                //select by value
                selectElement.SelectByValue(IOSValue);


            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("IOSDropdownError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " ISO dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [When(@"Select (.*) Utility from dropdown")]
        public void WhenSelectUtilityFromDropdown(string UtilityValue)
        {

            Thread.Sleep(2000);
            var isElementDisplayed = _driver.FindElement(By.Id("dropDownUtility")).Displayed;
            if (isElementDisplayed)
            {
                // select the drop down list
                var selectIOS = _driver.FindElement(By.Id("dropDownUtility"));
                //create select element object 
                var selectElement = new SelectElement(selectIOS);
                //select by value
                selectElement.SelectByText(UtilityValue);

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("UtilityDropdownError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Utility dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }


        [When(@"Select (.*) Congestion Zone from dropdown")]
        public void WhenSelectCongestionZoneFromDropdown(string congestionZoneValue)
        {

            Thread.Sleep(2000);
            var isElementDisplayed = _driver.FindElement(By.Id("dropDownCongestionZone")).Displayed;
            if (isElementDisplayed)
            {
                // select the drop down list
                var selectIOS = _driver.FindElement(By.Id("dropDownCongestionZone"));
                //create select element object 
                var selectElement = new SelectElement(selectIOS);
                //select by value
                selectElement.SelectByValue(congestionZoneValue);

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("congestionZoneDropdownError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Congestion zone dropdown is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [When(@"Enter Start Date greater than End Date")]
        public void WhenEnterStartDateGreaterThanEndDate()
        {


            DateTime now = DateTime.Now;
            var nextDate = DateTime.Now.AddMonths(1).ToString("MM/d/yyyy");

            var startDate = new DateTime(now.Year, now.Month, 1).ToString("MM/d/yyyy");

            Thread.Sleep(2000);
            var xpath = "//input[@value='" + startDate + "']";

            var isElementDisplayed = _driver.FindElement(By.XPath(xpath)).Displayed;
            if (isElementDisplayed)
            {
                _driver.FindElement(By.XPath(xpath)).Click();
                _driver.FindElement(By.XPath(xpath)).SendKeys(nextDate);
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("StartDateErrorError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Start date is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }


        [When(@"click on the Create Forecast search button")]
        public void WhenClickOnTheCreateForecastSearchButton()
        {

            var isElementDisplayed = _driver.FindElement(By.Id("webImageBtnSearch2")).Displayed;
            if (isElementDisplayed)
            {
                var submitBtn = _driver.FindElement(By.Id("webImageBtnSearch2"));
                submitBtn.Click();
            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("SubmitButtonError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Create button is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }


        [Then(@"The (.*) should be dispayed on the Create Forecast page")]
        public void ThenTheShouldBeDispayedOnTheCreateForecastPage(string message)
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("Label1")).Displayed;
            if (isElementDisplayed)
            {
                var actualMessage = _driver.FindElement(By.Id("Label1")).Text;
                Assert.AreEqual(actualMessage, message);
                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("CreateForecastPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("LabelMessage", By.Id("Label1"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("LabelMessageError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = " Message label is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }

        }

        [Then(@"ResultList should be dispayed on the Create Forecast page")]
        public void ThenResultListShouldBeDispayedOnTheCreateForecastPage()
        {

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            var isElementDisplayed = _driver.FindElement(By.Id("Table2")).Displayed;
            if (isElementDisplayed)
            {
                //maximize browser to get clear result screenshot


                //Take screenshots
                var pageScreenshotFilePath = _captureScreen.CapturePage("CreateForecastResultPage", _driver);
                var elementScreenshotFilePath = _captureScreen.CaptureErrorMessageElement("ResultList", By.Id("Table2"), _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));
                Console.WriteLine(new Uri(elementScreenshotFilePath));
                //resize browser tooriginal size

            }
            else
            {
                var pageScreenshotFilePath = _captureScreen.CapturePage("ResultError", _driver);
                Console.WriteLine(new Uri(pageScreenshotFilePath));

                //.........Send email or slack message for failed test starts
                var attachment = new List<FileInfo>
                {
                    new FileInfo(pageScreenshotFilePath)
                };

                var subject = ScenarioContext.Current.ScenarioInfo.Title + " failed";
                var content = "Result grid is not displayed on the page \n";
                content += "Feature Description: " + FeatureContext.Current.FeatureInfo.Description;

                var mailUtility = new MailUtility();
                mailUtility.SendEmailMessage(subject, content, attachment);
                //........end email or slack message for failed test ends

                Assert.Fail();
            }
        }
    }
}
