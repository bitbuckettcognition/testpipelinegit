﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-01 20:19:01 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2019-11-18 20:21:21
 */

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System.Collections.Generic;
using WES_TEST_Automation.Models;

namespace WES_TEST_Automation.WebDriverFactory
{
    /// <summary>
    /// Factory for creating WebDriver for various browsers.
    /// </summary>
    public class WebDriverFactory
    {
        /// <summary>
        /// Initilizes IWebDriver base on the given WebBrowser name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IWebDriver CreateWebDriver(WebBrowser name, bool runHeadless = false)
        {
            var commonStep = new CommonSteps(null);
            var config = commonStep.GetAppSettings();
            var WebDriverPath = config["WebDriverPath"];
            switch (name)
            {
                case WebBrowser.Edge:
                    var edgeOption = new EdgeOptions();
                    // edgeOption.AddArguments("--disable-extensions");
                    return new EdgeDriver(WebDriverPath, edgeOption);

                case WebBrowser.Firefox:
                    var firefoxOption = new FirefoxOptions();
                    firefoxOption.AddArguments("--disable-extensions");
                    // firefoxOption.AddArguments(new List<string>() { "no-sandbox", "headless", "disable-gpu", "--silent-launch", "--no-startup-window", });
                    return new FirefoxDriver(WebDriverPath, firefoxOption);
                case WebBrowser.IE:
                case WebBrowser.InternetExplorer:
                    var ieOption = new InternetExplorerOptions
                    {
                        IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                        EnsureCleanSession = true,
                        RequireWindowFocus = true
                    };
                    return new InternetExplorerDriver(WebDriverPath, ieOption);
                case WebBrowser.Chrome:
                default:
                    var chromeOption = new ChromeOptions();

                    if (runHeadless)
                    {
                        chromeOption.AddArguments(new List<string>() { "no-sandbox", "headless", "disable-gpu", "--silent-launch", "--no-startup-window", "--window-size=1920,1080", });
                    }
                    else
                    {
                        chromeOption.AddArguments(new List<string>() { "--disable-extensions", "--window-size=1920,1080", });
                    } 
                    return new ChromeDriver(WebDriverPath, chromeOption);
            }
        }
    }

    public enum WebBrowser
    {
        Edge,
        IE,
        InternetExplorer,
        Firefox,
        Chrome
    }
}
