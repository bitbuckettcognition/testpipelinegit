﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-04 20:23:24 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2019-11-18 20:23:44
 */

using System;
namespace WES_TEST_Automation.Models
{
    public class ValidatePassword
    {
        public bool CheckPasswordPattern(string password)
        {
            const int MIN_LENGTH = 12;
            const int MAX_LENGTH = 50;

            if (password == null)
            {
                throw new ArgumentNullException();
            }

            var meetsLengthRequirements = password.Length >= MIN_LENGTH && password.Length <= MAX_LENGTH;

            var trueCount = 0;

            if (meetsLengthRequirements)
            {
                foreach (var c in password)
                {
                    if (char.IsUpper(c)) { trueCount++; }
                    else if (char.IsLower(c)) { trueCount++; }
                    else if (char.IsSymbol(c)) { trueCount++; }
                    else if (char.IsNumber(c)) { trueCount++; }
                }
            }

            return trueCount >= 3;
        }
    }

}
