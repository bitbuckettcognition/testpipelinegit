﻿/*
 * @Author: sangram.bhore 
 * @Date: 2020-01-10 19:44:50 
 * @Last Modified by:   sangram.bhore 
 * @Last Modified time: 2020-01-14 19:44:50 
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace WES_TEST_Automation.Models
{
    class MailUtility
    {
        public void SendEmailMessage(string subject, string content, IList<FileInfo> attachment = null)
        {

            var fromAddresses = new List<EmailAddress>();
            var toAddresses = new List<EmailAddress>();

            var commonStep = new CommonSteps(null);
            var mailAddresses = commonStep.GetAppSettingsJson();
            var toAddressesList = mailAddresses["TestFailReportToEmailAddress"];

            //...........add to addresses
            for (var i = 0; i < toAddressesList.Count(); i++)
            {
                var toAddress = new EmailAddress
                {
                    Name = toAddressesList[i]["Name"].ToString(),
                    Address = toAddressesList[i]["Email"].ToString(),
                };
                toAddresses.Add(toAddress);
            }

            //.........add from adresses
            var fromAddressesList = mailAddresses["TestFailReportFromEmailAddress"];

            for (var i = 0; i < fromAddressesList.Count(); i++)
            {
                var fromAddress = new EmailAddress
                {
                    Name = fromAddressesList[i]["Name"].ToString(),
                    Address = fromAddressesList[i]["Email"].ToString(),
                };
                fromAddresses.Add(fromAddress);
            }

            var emailMessage = new EmailMessage
            {
                ToAddresses = toAddresses,
                FromAddresses = fromAddresses,
                Subject = subject,
                Content = content,
                Attachment = attachment
            };

            var config = commonStep.GetAppSettings();
            var mailRepository = new MailRepository(config["TestEmailSMTPHost"], Convert.ToInt32(config["TestEmailSMTPPort"]), Convert.ToBoolean(config["TestEmailSMTPSSL"]), config["TestEmailAddress"], config["TestEmailPassword"]);
            //var mailRepository = new MailRepository("smtp.office365.com", 587, false, config["TestEmailAddress"], config["TestEmailPassword"]);
            mailRepository.SendEmail(emailMessage);

        }
    }
}
