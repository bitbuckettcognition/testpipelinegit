﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-04 20:23:51 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:28
 */

using OpenQA.Selenium;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System;

namespace WES_TEST_Automation.Models
{
    public class CaptureScreen
    {
        public string CapturePage(string filename, IWebDriver driver)
        {
            var newname = filename + "_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss_ffffff_tt") + ".PNG";
            var commonStep = new CommonSteps(driver);
            var config = commonStep.GetAppSettings();

            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(config["ScreenCaptureFolderPath"] + newname, ScreenshotImageFormat.Png);
            return config["ScreenCaptureFolderPath"] + newname;
        }
        public string CaptureErrorMessageElement(string filename, By byElement, IWebDriver driver)
        {
            //SAVE IMAGE FIRST
            var newname = filename + "_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss_ffffff_tt") + ".PNG";
            var commonStep = new CommonSteps(driver);
            var config = commonStep.GetAppSettings();

            var imageFullPath = config["ScreenCaptureFolderPath"] + newname;
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(imageFullPath, ScreenshotImageFormat.Png);

            // 2. Get screenshot of specific element
            IWebElement element = driver.FindElement(byElement);
            var cropArea = new Rectangle(element.Location.X, element.Location.Y, element.Size.Width, element.Size.Height);

            //CROP IMAGE
            ImageCrop(imageFullPath, cropArea);

            return imageFullPath;
        }

        private void ImageCrop(string imageFullPath, Rectangle cropArea)
        { 
            using var image = (Image<Rgba32>)Image.Load(imageFullPath);
            image.Mutate(x => x.Crop(cropArea));
            image.Save(imageFullPath); // Automatic encoder selected based on extension.
        }
    }
}
