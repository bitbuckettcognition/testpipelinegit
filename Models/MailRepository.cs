﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-15 20:22:51 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:41
 */

using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Search;
using MimeKit;
using System.Collections.Generic;
using System.Linq;

namespace WES_TEST_Automation.Models
{
    class MailRepository
    {
        private readonly string _mailServer, _login, _password;
        private readonly int _port;
        private readonly bool _ssl;

        public MailRepository(string mailServer, int port, bool ssl, string login, string password)
        {
            _mailServer = mailServer;
            _port = port;
            _ssl = ssl;
            _login = login;
            _password = password;
        }


        public IEnumerable<string> GetUnreadMails()
        {
            var messages = new List<string>();

            using (var client = new ImapClient())
            {
                client.Connect(_mailServer, _port, _ssl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                _ = client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(_login, _password);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                _ = inbox.Open(FolderAccess.ReadOnly);
                var results = inbox.Search(SearchOptions.All, SearchQuery.Not(SearchQuery.Seen));
                foreach (var uniqueId in results.UniqueIds)
                {
                    var message = inbox.GetMessage(uniqueId);

                    messages.Add(message.HtmlBody);

                    //Mark message as read
                    inbox.AddFlags(uniqueId, MessageFlags.Seen, true);
                }

                client.Disconnect(true);
            }

            return messages;
        }

        public IEnumerable<string> GetAllMails()
        {
            var messages = new List<string>();

            using (var client = new ImapClient())
            {
                client.Connect(_mailServer, _port, _ssl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                _ = client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(_login, _password);

                // The Inbox folder is always available on all IMAP servers...
                var inbox = client.Inbox;
                _ = inbox.Open(FolderAccess.ReadWrite);

                var results = inbox.Search(SearchOptions.All, SearchQuery.And(SearchQuery.FromContains("security@energyservicesgroup.net"), SearchQuery.NotSeen));//SearchOptions.All, SearchQuery.NotSeen,
                foreach (var uniqueId in results.UniqueIds)
                {
                    var message = inbox.GetMessage(uniqueId);

                    messages.Add(message.TextBody);

                    //Mark message as read
                    inbox.AddFlags(uniqueId, MessageFlags.Seen, true);
                }

                client.Disconnect(true);
            }

            return messages;
        }

         

        public void SendEmail(EmailMessage emailMessage)
        {

            var message = new MimeMessage(); 

            message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

            message.Subject = emailMessage.Subject;
            

            var builder = new BodyBuilder
            {
                // Set the plain-text version of the message text
                TextBody = emailMessage.Content
            };

            if (emailMessage.Attachment != null)
            {
                foreach (var attachment in emailMessage.Attachment)
                {
                    // We may also want to attach a calendar event for Monica's party...
                    _ = builder.Attachments.Add(attachment.FullName);
                }
            }
            // Now we just need to set the message body and we're done
            message.Body = builder.ToMessageBody();

            //Be careful that the SmtpClient class is the one from Mailkit not the framework!
            using var client = new SmtpClient();
            //The last parameter here is to use SSL (Which you should!)
            client.Connect(_mailServer, _port, _ssl);
            //Remove any OAuth functionality as we won't be using it. 
            // the XOAUTH2 authentication mechanism.
            _ = client.AuthenticationMechanisms.Remove("XOAUTH2");
            client.Authenticate(_login, _password);
            client.Send(message);
            client.Disconnect(true);

        }
    }
}
