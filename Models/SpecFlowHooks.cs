﻿/*
 * @Author: sangram.bhore 
 * @Date: 2020-01-10 02:24:38 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:45:17
 */
using BoDi;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;
using WES_TEST_Automation.WebDriverFactory;

namespace WES_TEST_Automation
{
    [Binding]
    public sealed class SpecFlowHooks
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks
        private readonly IObjectContainer _container;

        public SpecFlowHooks(IObjectContainer container)
        {
            _container = container;
        }
         
        [BeforeScenario]
        public void CreateWebDriver()
        {
            // Create and configure a concrete instance of IWebDriver
            var webDriverFactory = new WebDriverFactory.WebDriverFactory();
            IWebDriver driver = webDriverFactory.CreateWebDriver(WebBrowser.Chrome);
             
            // Make this instance available to all other step definitions
            _container.RegisterInstanceAs(driver);
        }

        [AfterScenario]
        public void DestroyWebDriver()
        {
            IWebDriver driver = _container.Resolve<IWebDriver>();

            driver.Close();
            driver.Quit();
            driver.Dispose();
        }

        //[BeforeScenario]
        //public void BeforeScenario()
        //{
        //    //TODO: implement logic that has to run before executing each scenario
        //}

        //[AfterScenario]
        //public void AfterScenario()
        //{
        //    //TODO: implement logic that has to run after executing each scenario
        //}
    }
}
