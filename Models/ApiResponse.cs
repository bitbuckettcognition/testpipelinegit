/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-27 23:23:22 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:25
 */
using System;
using System.IO;
using System.Net;
using System.Text;

namespace WES_TEST_Automation.Models
{
    class ApiResponse
    {
        private readonly string _apiBaseUrlString = "<Base url of API's>"; //"http://107.180.78.163:8090/API/api/";
        public string GetResponse(string apiUrl)
        {
            var responseString = new StringBuilder();
            var ApiUrlString = string.Format(_apiBaseUrlString + "{0}", apiUrl);
            var request = (HttpWebRequest)WebRequest.Create(ApiUrlString);

            // Set some reasonable limits on resources used by this request
            request.MaximumAutomaticRedirections = 4;
            request.MaximumResponseHeadersLength = 4;
            // Set credentials to use for this request.
            request.Credentials = CredentialCache.DefaultCredentials;
            var response = (HttpWebResponse)request.GetResponse();

            try
            {
                // Get the stream associated with the response.
                Stream receiveStream = response.GetResponseStream();
                // Pipes the stream to a higher level stream reader with the required encoding format. 
                if (receiveStream != null)
                {
                    var readStream = new StreamReader(receiveStream, Encoding.UTF8);
                    var line = "";
                    while ((line = readStream.ReadLine()) != null)
                    {
                        _ = responseString.Append(line);
                    }
                    Console.WriteLine("Response stream received.");
                    Console.WriteLine(responseString);
                    response.Close();
                    readStream.Close();
                }
            }
            catch (Exception)
            {
                _ = responseString.Append("Did not get response");

            }
            return responseString.ToString();
        }
    }
}
