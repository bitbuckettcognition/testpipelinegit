﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-12-24 02:23:55 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:36
 */
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace WES_TEST_Automation.Models
{
    class ExcelUtility
    {
        public string CreateExcelReport(string fileName, string headerName, DataTable dtResultData, Dictionary<string, string> featureFileDetails)
        {
            var currentDate = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss_ffffff_tt");
            var newFile = @fileName + "_" + currentDate + ".xlsx";

            var featureTitle = featureFileDetails["featureTitle"];
            var featureDescription = featureFileDetails["featureDescription"];
            _ = featureFileDetails["scenarioTitle"];

            using (var fs = new FileStream(newFile, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet sheet1 = workbook.CreateSheet("Sheet1");

                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = HSSFColor.Grey25Percent.Index;
                styleHeader.FillPattern = FillPattern.SolidForeground;


                var fontHeader = workbook.CreateFont();
                fontHeader.Boldweight = 18;
                fontHeader.FontHeightInPoints = 18;
                styleHeader.SetFont(fontHeader);
                styleHeader.WrapText = true;
                styleHeader.Alignment = HorizontalAlignment.Center;
                styleHeader.VerticalAlignment = VerticalAlignment.Center;

                _ = sheet1.AddMergedRegion(new CellRangeAddress(0, 0, 0, 9));

                IRow excelRow = sheet1.CreateRow(0);
                excelRow.Height = 20 * 80;
                var cell2 = excelRow.CreateCell(0);
                cell2.SetCellValue(headerName + "\n " + DateTime.Now);
                cell2.CellStyle = styleHeader;


                //.........failed style

                var styleFailed = workbook.CreateCellStyle();
                styleFailed.FillForegroundColor = HSSFColor.Coral.Index;
                styleFailed.FillPattern = FillPattern.SolidForeground;


                //..........passed style 
                var stylePassed = workbook.CreateCellStyle();
                stylePassed.FillForegroundColor = HSSFColor.LightGreen.Index;
                stylePassed.FillPattern = FillPattern.SolidForeground;

                var rowcount = 3;
                //HEADER COLUMNS         


                var styleTableHeader = workbook.CreateCellStyle();
                styleTableHeader.FillForegroundColor = HSSFColor.Grey25Percent.Index;
                styleTableHeader.FillPattern = FillPattern.SolidForeground;
                styleTableHeader.Alignment = HorizontalAlignment.Center;
                styleTableHeader.VerticalAlignment = VerticalAlignment.Center;

                var fontTableHeader = workbook.CreateFont();
                fontTableHeader.Boldweight = 18;
                fontTableHeader.FontHeightInPoints = 12;
                styleTableHeader.SetFont(fontTableHeader);


                IRow rowSubHeader = sheet1.CreateRow(1);
                _ =
                    sheet1.AddMergedRegion(new CellRangeAddress(1, 1, 0, 2));
                var cellSubHeader = rowSubHeader.CreateCell(0);
                cellSubHeader.SetCellValue("Feature Title: " + featureTitle);


                _ = sheet1.AddMergedRegion(new CellRangeAddress(1, 1, 3, 9));
                var cellSubHeader2 = rowSubHeader.CreateCell(3);
                cellSubHeader2.SetCellValue("Description: " + featureDescription);


                IRow row = sheet1.CreateRow(rowcount);

                if (dtResultData.Columns.Count > 0)
                {
                    ICell currentCell;
                    //ADD INDEX
                    currentCell = row.CreateCell(0);
                    currentCell.SetCellValue("Serial No");
                    currentCell.CellStyle = styleTableHeader;
                    for (var i = 1; i <= dtResultData.Columns.Count; i++)
                    {
                        currentCell = row.CreateCell(i);
                        currentCell.SetCellValue(dtResultData.Columns[i - 1].ColumnName);
                        currentCell.CellStyle = styleTableHeader;
                        sheet1.SetColumnWidth(i, 30 * 256);
                    }
                }

                //DETAILS ROWS
                rowcount++;
                var index = 1;

                foreach (DataRow datarow in dtResultData.Rows)
                {
                    //Print Index
                    row = sheet1.CreateRow(rowcount);
                    ICell currentCell = row.CreateCell(0);
                    currentCell.SetCellValue(index);

                    if (datarow.Table.Columns.Contains("Test_Result"))
                    {
                        currentCell.CellStyle = datarow["Test_Result"].ToString() == "Fail" ? styleFailed : stylePassed;
                    }
                    index++;

                    //Print rows
                    for (var i = 1; i <= dtResultData.Columns.Count; i++)
                    {
                        currentCell = row.CreateCell(i);
                        currentCell.SetCellValue(datarow[i - 1].ToString());
                        if (datarow.Table.Columns.Contains("Test_Result"))
                        {
                            currentCell.CellStyle = datarow["Test_Result"].ToString() == "Fail" ? styleFailed : stylePassed;
                        }
                    }
                    rowcount++;
                }

                workbook.Write(fs);
            }

            return newFile;
        }
    }
}
