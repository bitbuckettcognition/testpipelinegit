﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WES_TEST_Automation.Models
{
    class EmailMessage
    {
        public EmailMessage()
        {
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        public List<EmailAddress> ToAddresses { get; set; }
        public List<EmailAddress> FromAddresses { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public IList<FileInfo> Attachment { get; set; }
    }
}
