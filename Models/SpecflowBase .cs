﻿/*
 * @Author: sangram.bhore 
 * @Date: 2020-01-10 02:24:32 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:45:13
 */
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace WES_TEST_Automation
{
    public class SpecflowBase
    {
        protected IWebDriver Driver { get; }
          
        public SpecflowBase(IWebDriver driver)
        {
            Driver = driver; 
        }

        //public void NavigateToURL(string url)
        //{
        //    var tags = ScenarioContext.Current.ScenarioInfo.Tags;
             
        //    Driver.Navigate().GoToUrl(url);
        //}
         
    }
}
