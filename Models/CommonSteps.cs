﻿/*
 * @Author: sangram.bhore 
 * @Date: 2019-11-13 20:22:15 
 * @Last Modified by: sangram.bhore
 * @Last Modified time: 2020-01-14 19:44:32
 */


using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using WES_TEST_Automation.FeatureSteps.Common;

namespace WES_TEST_Automation.Models
{
    public class CommonSteps:SpecflowBase
    {
        private WES_PortalLoginSteps _wes_PortalLoginSteps;
        private WES_NavigateToMenuSteps _navigateToMenuSteps;

        /// <summary>
        /// Method to call login steps 
        /// </summary>
        /// 

        readonly IWebDriver _driver;
        public CommonSteps(IWebDriver driver) : base(driver)
        {
            _driver = driver;

        }
        

        public IConfigurationRoot GetAppSettings()
        {

            var configPath = Path.GetFullPath("../appsettings.json");
            return new ConfigurationBuilder()
                        .AddJsonFile(configPath)
                        .Build();
        }
        public JObject GetAppSettingsJson()
        { 
            var configPath = File.ReadAllText(Path.GetFullPath("../appsettings.json"));
            return JObject.Parse(configPath);
        }


        public void PerformLogin()
        {
            var config = GetAppSettings();

            // start browser method 
            // _wes_PortalLoginSteps.StartBrowser();

            _wes_PortalLoginSteps = new WES_PortalLoginSteps(_driver);
            // common login methods to login into WES portal

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
            var redirectUrl = "";
            var username = "";
            var password = "";
            if (currentTags.Contains("WES_SNR_USTEST"))
            {
                redirectUrl = config["ESGPortalUrl_USTest"];
                username = config["TestESGPortalUsername_USTest"];
                password = config["TestESGPortalPassword_USTest"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
                redirectUrl = config["ESGPortalUrl_JPTest"];
                username = config["TestESGPortalUsername_JPTest"];
                password = config["TestESGPortalPassword_JPTest"];

            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
                redirectUrl = config["ESGPortalUrl_USProd"];
                username = config["TestESGPortalUsername_USProd"];
                password = config["TestESGPortalPassword_USProd"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            {
                redirectUrl = config["ESGPortalUrl_JPProd"];
                username = config["TestESGPortalUsername_JPProd"];
                password = config["TestESGPortalPassword_JPProd"];
            }

            _wes_PortalLoginSteps.GivenINavigateToTheWESPortal(redirectUrl); 
            _wes_PortalLoginSteps.GivenILoginToTheWESWithAndOnLoginPage(username, password);
            _wes_PortalLoginSteps.WhenIClickedOnTheLoginButton();
        }

        public void ExitDriver() {
            _driver.Close();
            _driver.Quit();
        }

        /// <summary>
        /// Method to perform navigation handling steps
        /// </summary>
        
        public void PerformMenuNavigation(string menuItem, string menuOption)
        {
            //common navigation to redirect to particular page
            _navigateToMenuSteps = new WES_NavigateToMenuSteps(_driver);

            _navigateToMenuSteps.GivenIHoverTo(menuItem);
            _navigateToMenuSteps.WhenIClickOnTheDropdown(menuOption);
        }

        // Update password in config file
        public void UpdatePasswordInConfig(string tempPasswordHolder)
        {
            var commonStep = new CommonSteps(_driver);
            var config = commonStep.GetAppSettings();

            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
           
            var passwordBeforeChange = "";
            if (currentTags.Contains("WES_SNR_USTEST"))
            {
               
                passwordBeforeChange = config["TestESGPortalPassword_USTest"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
               
                passwordBeforeChange = config["TestESGPortalPassword_JPTest"];

            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
               
                passwordBeforeChange = config["TestESGPortalPassword_USProd"];
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            { 
                passwordBeforeChange = config["TestESGPortalPassword_JPProd"];
            }
             
            var configFilePath = config["AppConfigFilePath"];
            var text = File.ReadAllText(configFilePath);
            text = text.Replace(passwordBeforeChange, tempPasswordHolder);
            File.WriteAllText(configFilePath, text);
        }
    }
}
