﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using TechTalk.SpecFlow;

namespace WES_TEST_Automation.Models
{
    class OracleDataUtility : IDisposable
    {
        readonly OracleConnection _dbConnection;
        //Connect the database
        public OracleDataUtility()
        {
            var commonStep = new CommonSteps(null);
            var config = commonStep.GetAppSettings();

             
            var currentTags = ScenarioContext.Current.ScenarioInfo.Tags;
            var dBConnectionString = "";
            
            if (currentTags.Contains("WES_SNR_USTEST"))
            {
                dBConnectionString = config["DBConnectionString_USTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANTEST"))
            {
                dBConnectionString = config["DBConnectionString_JPTest"]; 
            }
            else if (currentTags.Contains("WES_SNR_USPROD"))
            {
                dBConnectionString = config["DBConnectionString_USProd"]; 
            }
            else if (currentTags.Contains("WES_SNR_JAPANPROD"))
            {
                dBConnectionString = config["DBConnectionString_JPProd"]; 
            }

            var dbConn = dBConnectionString.ToString();
            _dbConnection = new OracleConnection(dbConn);
            _dbConnection.Open();
        }

        ~OracleDataUtility()
        {
            _dbConnection.Close();
        }
        public void Dispose()
        {

        }

        public DataTable GetDataTableFromQueryString(string queryString)
        {
            var dtResult = new DataTable();

            var oCmd = new OracleCommand(queryString, _dbConnection);
            var dataAdapter = new OracleDataAdapter
            {
                SelectCommand = oCmd
            };
            var dataSet = new DataSet();
            _ = dataAdapter.Fill(dataSet);

            if (dataSet.Tables[0] != null)
            {
                dtResult = dataSet.Tables[0];
            }

            return dtResult;
        }
    }
}
